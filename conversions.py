import numpy as np

Grav_C      = 4.302e3            # pc M_sun^-1 (m s^-1)^2
H0          = 70.4               # km s^-1 Mpc^-1
Au_to_Pc    = 4.84814e-6         # pc au^-1
Pc_to_m     = 3.086e16           # m pc^-1
Pc_to_Au    = 206265.            # Au pc^-1
Ly_to_Pc    = 0.306601           # pc ly^-1
Au_to_m     = 1.496e11           # m au^-1
D_sun       = 1.58e-5 * Ly_to_Pc # pc
c           = 299792.458         # km s^-1

Abs_Mag_Sun = {'u': 6.39,
               'g': 5.11,
               'r': 4.65,
               'i': 4.53,
               'z': 4.50,
               'U': 6.33,
               'B': 5.31,
               'V': 4.80,
               'R': 4.60,
               'I': 4.51,
               'J': 4.54,
               'H': 4.66,
               'K': 5.08,
               '3.6um': 3.24}     # mips.as.arizona.edu/~cnaw/sun.html # also see: http://www.astronomy.ohio-state.edu/~martini/usefuldata.html
                       #3.24??? came from Lelli+2016
# old version, incorrect?
# Abs_Mag_Sun = {'u': 6.77,
#                'g': 5.36,
#                'r': 4.67,
#                'i': 4.48,
#                'z': 4.42,
#                'U': 5.55,
#                'B': 5.45,
#                'V': 4.80,
#                'R': 4.46,
#                'I': 4.11,
#                'J': 3.67,
#                'H': 3.33,
#                'K': 3.29,
#                '3.6um': 3.24}     # mips.as.arizona.edu/~cnaw/sun.html
asinh_softening = {'u': 1.4e-10,
                   'g': 9e-11,
                   'r': 1.2e-10,
                   'i': 1.8e-10,
                   'z': 7.4e-10} # http://www.sdss.org/dr12/algorithms/magnitudes/

HubbleTypeConversion_deVacouleurs = {'cE': -6,
                                     'E': -5,
                                     'E+':-4,
                                     'S0-':-3,
                                     'S0':-2,
                                     'S0+':-1,
                                     'S0/a':0,
                                     'Sa':1,
                                     'Sab':2,
                                     'Sb':3,
                                     'Sbc':4,
                                     'Sc':5,
                                     'Scd':6,
                                     'Sd':7,
                                     'Sdm':8,
                                     'Sm':9,
                                     'Im':10} #https://en.wikipedia.org/wiki/Galaxy_morphological_classification

HubbleTypeConversion_Hubble = {'E': -4,
                               'S0':-1,
                               'S0/a':0,
                               'Sa':1,
                               'Sa-b':2,
                               'Sb':3,
                               'Sb-c':4,
                               'Sc':7,
                               'Sc-Irr':8,
                               'Sc-Ir':8,
                               'Ir I':10,
                               'Ir':10,
                               'Irr':10,
                               'Irr I':10}

def ConvertHubbleType(HT, HT_type = None):
    special = {'Sb/Sc':4, 'S':1}
    not_needed = 'BpLN?():'
    for c in not_needed:
        HT = HT.replace(c,'')
    HT = HT.strip(' \n')
        
    if HT in special:
        return special[HT]
    
    if HT_type == 'deVacouleurs':
        try:
            return HubbleTypeConversion_deVacouleurs[HT]
        except:
            if 'D' in HT:
                return 11
            else:
                print('unidentified Hubble Type!: ', HT)
                return 12
    elif HT_type == 'Hubble':
        try:
            return HubbleTypeConversion_Hubble[HT]
        except:
            if 'D' in HT:
                return 11
            elif 'Ir' in HT:
                return 10
            else:
                print('unidentified Hubble Type!: ', HT)
                return 12
    else:
        try:
            return HubbleTypeConversion_deVacouleurs[HT]
        except:
            pass
        try:
            return HubbleTypeConversion_Hubble[HT]
        except:
            pass
        if 'Ir' in HT:
            return 10
        if 'D' in HT:
            if 'E' in HT:
                return -8
            if 'S' in HT:
                return 11
        print('unidentified Hubble Type!: ' + HT)
        return 12
        
            
def mag_to_L(mag, band, mage = None, zeropoint = None):
    """
    Returns the luminosity (in solar luminosities) given the absolute magnitude and reference point.
    mag: Absolute magnitude
    band: Photometric band in which measurements were taken
    mage: uncertainty in absolute magnitude
    zeropoint: user defined zero point
    returns: Luminosity in solar luminosities
    """

    L = 10**(((Abs_Mag_Sun[band] if zeropoint is None else zeropoint) - mag)/2.5)
    if mage is None:
        return L
    else:
        Le = np.abs(L * mage * np.log(10) / 2.5)
        return L, Le

def L_to_mag(L, band, Le = None, zeropoint = None):
    """
    Returns the Absolute magnitude of a star given its luminosity and distance
    L: Luminosity in solar luminosities
    band: Photometric band in which measurements were taken
    Le: Uncertainty in luminosity
    zeropoint: user defined zero point
    
    returns: Absolute magnitude
    """

    mag = (Abs_Mag_Sun[band]if zeropoint is None else zeropoint) - 2.5 * np.log10(L)
    
    if Le is None:
        return mag
    else:
        mage = np.abs(2.5 * Le / (L * np.log(10)))
        return mag, mage

def app_mag_to_abs_mag(m, D, me = 0., De = 0.):
    """
    Converts an apparent magnitude to an absolute magnitude
    m: Apparent magnitude
    D: Distance to object in parsecs
    returns: Absolute magnitude at 10 parcecs
    """

    M = m - 5.0 * np.log10(D / 10.0)
    if np.all(me == 0) and np.all(De == 0):
        return M
    else:
        return M, np.sqrt(me**2 + (5. * De / (D * np.log(10)))**2)

def abs_mag_to_app_mag(M, D, Me = 0., De = 0.):
    """
    Converts an absolute magnitude to an apparent magnitude
    M: Absolute magnitude at 10 parcecs
    D: Distance to object in parsecs
    returns: Apparent magnitude
    """

    m = M + 5.0 * np.log10(D / 10.0)
    if np.all(Me == 0) and np.all(De == 0):
        return m
    else:
        return m, np.sqrt(Me**2 + (5. * De / (D * np.log(10)))**2)

def magperarcsec2_to_mag(mu, a = None, b = None, A = None):
    """
    Converts mag/arcsec^2 to mag
    mu: mag/arcsec^2
    a: semi major axis radius (arcsec)
    b: semi minor axis radius (arcsec)
    A: pre-calculated area (arcsec^2)
    returns: mag
    """
    assert (not A is None) or (not a is None and not b is None)
    if A is None:
        A = np.pi * a * b
    return mu - 2.5*np.log10(A) # https://en.wikipedia.org/wiki/Surface_brightness#Calculating_surface_brightness

def mag_to_magperarcsec2(m, a = None, b = None, R = None, A = None):
    """
    Converts mag to mag/arcsec^2
    m: mag
    a: semi major axis radius (arcsec)
    b: semi minor axis radius (arcsec)
    A: pre-calculated area (arcsec^2)
    returns: mag/arcsec^2
    """
    assert (not A is None) or (not a is None and not b is None) or (not R is None)
    if not R is None:
        A = np.pi * (R**2)
    elif A is None:
        A = np.pi * a * b
    return m + 2.5*np.log10(A) # https://en.wikipedia.org/wiki/Surface_brightness#Calculating_surface_brightness

def halfmag(mag):
    """
    Computes the magnitude corresponding to half in log space.
    Effectively, converts to luminosity, divides by 2, then
    converts back to magnitude. Distance is not needed as it
    cancels out. Here is a basic walk through:
    m_1 - m_ref = -2.5log10(I_1/I_ref)
    m_2 - m_ref = -2.5log10(I_1/2I_ref)
                = -2.5log10(I_1/I_ref) + 2.5log10(2)
    m_2 = m_1 + 2.5log10(2)
    """

    return mag + 2.5 * np.log10(2)

def L_to_Flux(L, D):
    """
    Converts luminosity into the observed flux at a given distance.
    L: Luminosity in solar luminosities
    D: Distance to star in parsecs
    returns: Flux at distance D
    """

    return L / (4.0 * np.pi * (D**2))

def Flux_to_L(Flux, D):
    """
    Converts the observed flux into luminosity at a given distance.
    F: Flux at distance D
    D: Distance to star in parsecs
    returns: Luminosity in solar luminosities
    """

    return Flux * 4.0 * np.pi * (D**2)

def mag_to_flux_asinh(m, band):
    """
    Converts an sdss magnitude into fixme
    http://www.sdss.org/dr12/algorithms/fluxcal/
    http://www.sdss.org/dr12/algorithms/magnitudes/
    https://en.wikipedia.org/wiki/AB_magnitude
    """
    assert band in 'ugriz'

    b = asinh_softening[band]

    return 2. * b * np.arcsinh( - m * np.log(10) / 2.5 - np.log(b) )

def flux_to_mag_asinh(f_f0, band):

    assert band in 'ugriz'

    b = asinh_softening[band]

    return 2.5 * (np.arcsinh(f_f0 / (2. * b)) + np.log(b)) / np.log(10)

def pc_to_arcsec(R, D, Re = 0.0, De = 0.0):
    """
    Converts a size in parsec to arcseconds

    R: length in pc
    D: distance in pc
    """

    theta = R / (D * Au_to_Pc)
    if np.all(Re == 0) and np.all(De == 0):
        return theta
    else:
        e = theta * np.sqrt((Re/R)**2 + (De/D)**2)
        return theta, e

def arcsec_to_pc(theta, D, thetae = 0.0, De = 0.0):
    """
    Converts a size in arcseconds to parsec

    theta: angle in arcsec
    D: distance in pc
    """
    r = theta * D * Au_to_Pc
    if np.all(thetae == 0) and np.all(De == 0):
        return r
    else:
        e = r * np.sqrt((thetae / theta)**2 + (De / D)**2)
        return r, e

def ISB_to_muSB(I, band, IE = None):
    """
    Converts surface brightness in Lsolar pc^-2 into mag arcsec^-2

    I: surface brightness, (L/Lsun) pc^-2
    band: Photometric band in which measurements were taken
    returns: surface brightness in mag arcsec^-2
    """

    muSB = 21.571 + Abs_Mag_Sun[band] - 2.5 * np.log10(I)
    if IE is None:
        return muSB
    else:
        return muSB, (2.5/np.log(10)) * IE / I

def muSB_to_ISB(mu, band, muE = None):
    """
    Converts surface brightness in mag arcsec^-2 into Lsolar pc^-2

    mu: surface brightness, mag arcsec^-2
    band: Photometric band in which measurements were taken
    returns: surface brightness in (L/Lsun) pc^-2
    """

    ISB = 10**((21.571 + Abs_Mag_Sun[band] - mu)/2.5)
    if muE is None:
        return ISB
    else:
        return ISB, (np.log(10)/2.5) * ISB * muE

def muSBE_to_ISBE(mu, muE, band):
    """
    Converts an uncertainty from surface brightness to intensity

    mu: surface brightness, mag arcsec^-2
    muE: surface brightness uncertainty (unitless)
    band: Photometric band in which measurements were taken
    returns: intensity uncertainty (L/Lsun) pc^-2
    """

    return (np.log(10)/2.5) * muSB_to_ISB(mu, band) * muE

def ISBE_to_muSBE(I, IE, band):
    """
    Converts an uncertainty from intensity to surface brightness

    I: intensity (L/Lsun) pc^-2
    IE: intensity uncertainty (L/Lsun) pc^-2
    band: Photometric band in which measurements were taken
    returns: surface brightness uncertainty (unitless)
    """

    return (2.5/np.log(10)) * IE / I
    

def COG_to_SBprof(R, COG, band):
    """
    Compute a surface brightness profile from a curve of growth.

    R: Radius array values (pc)
    COG: mag array values (mag)
    """

    assert np.all(COG[:-1] - COG[1:] > 0)
    assert len(R) == len(COG)
    print( R, COG)
    ISB = [mag_to_L(COG[0], band = band) / (np.pi * R[0]**2)]
    for i in range(1,len(R)):
        ISB.append((mag_to_L(COG[i], band = band) - mag_to_L(COG[i-1], band = band)) / (np.pi * (R[i]**2 - R[i-1]**2)))
    # SB = [mag_to_magperarcsec2(COG[0],R = R[0])]

    # for i in range(1,len(R)):
    #     SB.append(mag_to_magperarcsec2(L_to_mag(mag_to_L(COG[i], zeropoint = 15) - mag_to_L(COG[i-1], zeropoint = 15), zeropoint = 15), A = np.pi*(R[i]**2 - R[i-1]**2)))

    return ISB_to_muSB(np.array(ISB), band = band)

def vcmb_to_z(vcmb):
    """
    computes the redshift using the cmb velocity which is purely in the radial direction

    vcmb: the velocity of an object along the line of sight (km s^-1)

    returns: z the redshift (unitless)
    """

    # https://en.wikipedia.org/wiki/Redshift#Redshift_formulae
    return np.sqrt((1. + (vcmb / c)) / (1. - (vcmb / c))) - 1.

def z_to_vcmb(z):
    """
    computes the the cmb velocity which is purely in the radial direction from redshift

    z: redshift (unitless)

    returns: vcmb the velocity of an object along the line of sight (km s^-1)
    """

    # https://en.wikipedia.org/wiki/Redshift#Redshift_formulae
    return c * ((1. + z)**2 - 1.) / ((1. + z)**2 + 1.)

from conversions import H0

def DistUncert(D):
    """
    D: distance in Mpc
    """

    return max(D * 0.5, 500. / H0) 

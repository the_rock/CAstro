import matplotlib.pyplot as plt
import numpy as np

def SB_plot(R, SB, band = None, close = True, add_to_axis = None, share_x_axis = None, message = '', saveas = None, invert_axis = True, xunit = 'arcsec', colour = 'b'):
    """
    Plots a surface brightness profile
    """

    assert len(R) == len(SB)
    
    if add_to_axis is None and share_x_axis is None:
        f, ax = plt.subplots()
    elif not add_to_axis is None:
        f, ax = add_to_axis
    elif not share_x_axis is None:
        f = share_x_axis[0]
        ax = share_x_axis[1].twinx()

    ax.scatter(R, SB, label = ('Surface Brightness%s' % ('' if band is None else ' band: %s' % band)) if message == '' else message, c = colour)
    ax.set_xlabel('Radius (%s)' % xunit)
    ax.set_ylabel('Surface Brightness (mag arcsec${}^{-2}$)')
    if invert_axis:
        ax.invert_yaxis()

    if not saveas is None:
        plt.savefig(saveas)

    if close:
        plt.close(f)

    return (f,ax)


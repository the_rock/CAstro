import numpy as np


# Get V magnitude
#---------------------------------------------------------------------
def V_model(r, theta):
    """
    Model for velocity magnitude as a function of position in the disk.
    Change this to change how the V(R, theta) model is represented

    r: position in disk with r[0] = radius and r[1] = angle/phi
    theta: parameters for the model

    returns: Velocity magnitude
    """
    return theta[0] + theta[1]*np.arctan(theta[2]*r[0] + theta[3])

def Inclined_Disk(x, i):
    """
    Computes the R, phi coordinates in the face-on view space of the galaxy from the projected coordinates x

    x: projected coordinates corrected for position angle with x[0] = x and x[1] = y
    i: inclination of the galaxy with i = 0 as face-on and i = pi/2 as edge-on

    returns: R, phi coordinates in face-on view
    """

    Z = x[0]*np.tan(i)
    R = np.sqrt(x[0]**2 + x[1]**2 + Z**2)

    phi = np.arccos(x[1] / R) + (np.pi * (x[0] < 0))

    return R, phi

def Rotated_Disk(x, PA):
    """
    Correct projected sky coordinates for position angle so that inclination is a rotation about y-axis.

    x: projected coordinates on the sky with x[0] = x and x[1] = y
    PA: Angle clockwise from north of principal axis of galaxy

    returns: x_prime, y_prime, projected coordinates corrected with principal axis aligned with the y-axis
    """

    xprime = x[0]*np.cos(-PA) - x[1]*np.sin(-PA)
    yprime = x[0]*np.sin(-PA) + x[1]*np.cos(-PA)

    return [xprime, yprime]
#---------------------------------------------------------------------


# Get line-of-sight component of velocity unit vector
#---------------------------------------------------------------------
def On_Galaxy_Coordinates(x, PA, i):
    """
    Compute on galaxy coordinates from the projected coordinates, position angle, and inclination of a galaxy

    x: on sky projected coordinates
    PA: Angle clockwise from north of principal axis of galaxy
    i: inclination of the galaxy with i = 0 as face-on and i = pi/2 as edge-on

    returns: x_prime, y_prime, deprojected coordinates on the galaxy plane
    """
    R, phi = Inclined_Disk(Rotated_Disk(x, PA), i)

    x_gal = R * np.cos(phi)
    y_gal = R * np.sin(phi)

    return [x_gal, y_gal]

def LOS_component(x, i):
    """
    Compute the component of the velocity unit vector in the line-of-sight direction given the galaxy plane coordinates and inclination

    x: deprojected coordinates on the galaxy plane
    i: inclination of the galaxy with i = 0 as face-on and i = pi/2 as edge-on

    returns: component of the velocity unit-vector in the line-of-sight
    """
    return np.sin(i) * x[1] / np.sqrt(x[0]**2 + x[1]**2)
#---------------------------------------------------------------------

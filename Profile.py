import numpy as np
from scipy.optimize import minimize
from scipy.special import gammainc
from .conversions import ISB_to_muSB, muSB_to_ISB, Abs_Mag_Sun, halfmag
from .models import sersic_log, sersic_I
import matplotlib.pyplot as plt
import sys
import os
sys.path.append(os.environ['PYTHON_GOODIES'])
from Smoothing import PolySmooth, fit_two_points
from goodies.python_goodies.Uncertainty import Bootstrap, StatisticalMeasuresOnX


######################################################################
def Evaluate_Surface_Brightness(R, SB, atR, E = None):
    """
    Evaluate a smoothed version of a surface brightness profile at
    any radius "atR". For values smaller than min(R) or greater than
    max(R), a linear extrapolation is done with numpy.polyfit on the
    inner 5 points or outer 25% of the profile. Within range(R),
    numpy.interp is used.

    R: Radius of the profile [any units]
    SB: Surface brightness [mag arcsec^-2]
    atR: Radius where SB profile should be evaluated [same units as R]
         if atR is an iterable, this function will call itself recursively
    E: Uncertainty in Surface Brightness [unitless sigma in mag arcsec^-2 space]

    returns: Surface Brightness at "atR"
    """

    if hasattr(atR, '__len__'):
        return list(map(Evaluate_Surface_Brightness, [R]*len(atR), [SB]*len(atR), atR, [E]*len(atR)))

    if atR < R[0]:
        r = np.polyval(np.polyfit(R[:5],SB[:5],1,w = None if E is None else 1./np.clip(E[:5], a_min = 0.001, a_max = None)), max(atR,0.))
    elif atR > R[-1]:
        start = int(len(R) * 3. / 4.)
        r = np.polyval(np.polyfit(R[start:],SB[start:],1,w = None if E is None else 1./np.array(E[start:])), atR)
    else:
        r = np.interp(atR, R, SB)
    if E is None:
        return r
    else:
        if atR < R[0]:
            e = np.mean(E[:5])
        elif atR > R[-1]:
            e = np.mean(E[start:])
        else:
            e = E[np.argmin(np.abs(R - r))]
        if e < 0:
            print('How is this possible??? negative error???', R, SB, atR, E)
        return r, e

######################################################################
def Evaluate_Magnitude(R, m, atR, E = None):
    """
    Evaluate a smoothed version of a curve of growth at
    any radius "atR". For values smaller than min(R), a linear
    extrapolation is done with numpy.polyfit on the
    inner 5 points. For values greater than max(R), the minimum
    m value is returned. Within range(R), numpy.interp is used.

    R: Radius of the profile [any units]
    m: magnitude enclosed within radius R [mag]
    atR: Radius where SB profile should be evaluated [same units as R]
         if atR is an iterable, this function will call itself recursively
    E: Uncertainty in magnitude [unitless sigma in mag space]

    returns: Magnitude at "atR"
    """

    if hasattr(atR, '__len__'):
        return list(map(Evaluate_Magnitude, [R]*len(atR), [m]*len(atR), atR, [E]*len(atR)))
    
    if R[0] == 0:
        start = 1
    else:
        start = 0
            
    if atR < R[start]:
        matR = np.polyval(np.polyfit(np.log10(np.abs(R[start:5+start])),m[start:5+start],1,w = None if E is None else 1./np.clip(E[start:5+start], a_min = 0.001, a_max = None)), np.log10(max(atR,1e-3)))
        if E is None:
            return matR
        else:
            return matR, E[start]
    elif atR > R[-1]:
        if E is None:
            return min(m[start:])
        else:
            return min(m[start:]), E[np.argmin(m[start:])]
    else:
        matR = np.interp(atR, R[start:], m[start:])
        if E is None:
            return matR
        else:
            return matR, np.interp(atR, R[start:], E[start:])


def DiskScaleLength(sb_R, sb_SB, band):

    p = np.polyfit(sb_R[-15:-2], sb_SB[-15:-2], deg = 1)

    return {'Rd': 2.5 * np.log10(np.e) / p[0], 'I0': 10**((21.571 + Abs_Mag_Sun[band] - p[1])/2.5)}
    
def EffectiveRadius(cog_R, cog_COG, cog_E = None, ratio = 0.5, force_total_mag = None):

    mag_effective = (min(cog_COG) if force_total_mag is None else force_total_mag) + 2.5 * np.log10(1./ratio) #halfmag(cog_COG[-1])
    Re = None
    ReE = None
    for i in range(1,len(cog_R)):
        if cog_COG[i] < mag_effective < cog_COG[i-1]:
            Re = fit_two_points(cog_COG[i-1], cog_R[i-1], cog_COG[i], cog_R[i], mag_effective)
            # lamb = (mag_effective - cog_COG[i]) / (cog_COG[i-1] - cog_COG[i])
            # Re = lamb * cog_R[i-1] + (1. - lamb) * cog_R[i]
            if not cog_E is None:
                ReE = abs(np.sqrt(np.mean(cog_E[max(i-2, 0):min(len(cog_R),i+3)]**2)) * (cog_R[i] - cog_R[i-1]) / (cog_COG[i] - cog_COG[i-1]))
                
    if cog_E is None:
        return Re
    else:
        return Re, ReE

def wrap_RE(X):

    N = np.argsort(X[:,0])
    return EffectiveRadius(X[:,0][N], X[:,1][N], X[0,2], X[0,3])

def EffectiveRadiusError(cog_R, cog_COG, cog_E, ratio = 0.5, force_total_mag = None):

    if len(cog_R) == 0 or len(cog_R) != len(cog_COG) or len(cog_R) != len(cog_E):
        print('Failed Checks')
        return None

    res = Bootstrap(np.array(zip(cog_R, cog_COG, np.ones(len(cog_R))*ratio, [force_total_mag]*len(cog_R))), wrap_RE, procs = 8, n = 500)
    CHOOSE = []
    for r in res['evaluations']:
        if r is None:
            CHOOSE.append(False)
        else:
            CHOOSE.append(True)

    if np.sum(CHOOSE) < 10:
        return None
    stats = StatisticalMeasuresOnX(np.array(res['evaluations'])[CHOOSE])

    return (stats['1sigma range'][1] - stats['1sigma range'][0])/2.
    
    
def IsophotalRadius(sb_R, sb_SB, mu, sb_E = None, extrapolate = False):

    if len(sb_R) == 0:
        return None

    Riso = None

    for i in range(1,len(sb_R)):
        if sb_SB[i] > mu > sb_SB[i-1]:
            R = sb_R[max(i-2, 0):min(len(sb_R),i+3)]
            SB = sb_SB[max(i-2, 0):min(len(sb_R),i+3)]
            p = np.polyfit(R,SB,deg=1)
            Riso = (mu - p[1]) / p[0] #fit_two_points(sb_SB[i-1], sb_R[i-1], sb_SB[i], sb_R[i], mu)
            # lamb = (mu - sb_SB[i]) / (sb_SB[i-1] - sb_SB[i])
            # Riso = lamb * sb_R[i-1] + (1. - lamb) * sb_R[i]
            if not sb_E is None:
                Risoe = abs(np.sqrt(np.mean(sb_E[max(i-2, 0):min(len(sb_R),i+3)]**2)) / p[0]) #(sb_R[i] - sb_R[i-1]) / (sb_SB[i] - sb_SB[i-1]))
            break

    if extrapolate and Riso is None:
        print('extrapolating')

        R = sb_R[-int(len(sb_R)/4):] if np.all(np.array(sb_SB) < mu) else sb_R[:int(len(sb_R)/4)]
        SB = sb_SB[-int(len(sb_R)/4):] if np.all(np.array(sb_SB) < mu) else sb_SB[:int(len(sb_R)/4)]
        if not sb_E is None:
            E = sb_E[-int(len(sb_R)/4):] if np.all(np.array(sb_SB) < mu) else sb_E[:int(len(sb_R)/4)]
        p = np.polyfit(R, SB, deg = 1)
        if p[0] < 0.001 or sb_SB[-1] < sb_SB[-2]:
            print('WARNING: profile rising, trying with 2 less elements')
            return IsophotalRadius(sb_R[:-2], sb_SB[:-2], mu, extrapolate = extrapolate, sb_E = sb_E)
        Riso = (mu - p[1]) / p[0]
        if not sb_E is None:
            Risoe = abs(np.sqrt(np.mean(E**2)) / p[0])
            
    if sb_E is None:
        return Riso
    else:
        return Riso, Risoe

# def fitpoly(X):
#     pp = np.polyfit(X[:,0], X[:,1], deg = 1)
#     return (X[0][2] - pp[1])/pp[0]

# def IsophotalRadiusError(sb_R, sb_SB, sb_E, mu, zero_point_err = 0.1, extrapolate = False):

#     if len(sb_R) == 0:
#         return None
#     Rerr = None

#     for i in range(1,len(sb_R)):
#         if sb_SB[i] > mu > sb_SB[i-1]:
#             start = max(i - 5, 0)
#             end = min(i + 5, len(sb_R))
#             m = np.polyfit(sb_R[start:end], sb_SB[start:end], 1)[0]
#             # m = (sb_SB[i] - sb_SB[i-1]) / (sb_R[i] - sb_R[i-1])
#             print('slope: ' + str(m) + str(np.mean(sb_E[start:end])))
#             Rerr = np.mean(sb_E[start:end]) / m
#             break

#     if extrapolate and Rerr is None:
#         print('extrapolating')
#         p = np.polyfit(sb_R[-int(len(sb_R)/4.):], sb_SB[-int(len(sb_R)/4.):], deg = 1)
#         if p[0] < 0 or sb_SB[-1] < sb_SB[-2]:
#             print('WARNING: profile rising, trying with 2 less elements')
#             return IsophotalRadiusError(sb_R[:-2], sb_SB[:-2], sb_E[:-2], mu, extrapolate = extrapolate)
#         res = Bootstrap(np.array(zip(sb_R[-int(len(sb_R)/3.):], sb_SB[-int(len(sb_R)/3.):], mu*np.ones(30))), fitpoly, n = 250, procs = 8)
#         Rerr = np.sqrt((abs(res['1sigma range'][1] - res['1sigma range'][0])/2.)**2 + (np.median(sb_E[-int(len(sb_R)/4.):])/p[0])**2)
#     print('Rerr: ' + str(Rerr))
#     if Rerr > 50:
#         Rerr = '-'
#     return Rerr

def Extrapolate_COG(R, m):

    R = np.array(R)
    m = np.array(m)
    
    m = m[R != 0]
    R = R[R != 0]

    Re = EffectiveRadius(R, m)

    half = int(len(R)/2.)
    pastre = R > Re
    if np.sum(pastre) < 6:
        pastre = R >= R[-min(6, len(R)-1)]
    
    f3 = lambda x: np.mean((m[pastre] - (x[0] - 2.5*np.log10(1. - (x[1]*R[pastre]+1.)*np.exp(-x[1]*R[pastre]))))**2)

    f3_fun = lambda x, r: x[0] - 2.5*np.log10(1. - (x[1]*r+1.)*np.exp(-x[1]*r))

    res1 = minimize(f3, x0 = [m[-1], 1./R[np.argmax(-m)]], method = 'Nelder-Mead')
    res2 = minimize(f3, x0 = [m[-1], 100./R[np.argmax(-m)]], method = 'Nelder-Mead')
    res3 = minimize(f3, x0 = [m[-1], 10./R[np.argmax(-m)]], method = 'Nelder-Mead')

    res = [res1, res2, res3][np.argmin([res1.fun, res2.fun, res3.fun])]

    return res.x[0]

def _bootstrap_extrapolateCOG_Error(X):
    return Extrapolate_COG(X[:,0], X[:,1])

def Extrapolate_COG_Error(R, m, me):
    res = Bootstrap(np.array(zip(R,m)), _bootstrap_extrapolateCOG_Error, n = 200, procs = 8)
    return (res['1sigma range'][1] - res['1sigma range'][0])/2.

def compute_Total_Apparent_Magnitude(m, sbE, return_radius = False):
    CHOOSE = np.logical_and(m < 27, sbE < 0.15)
    if np.sum(CHOOSE) <= 5:
        return '-','-'

    for i in range(int(len(CHOOSE)/2),max(len(CHOOSE)-5, int(len(CHOOSE)/2))):
        if np.all(np.logical_not(CHOOSE[i:i+5])):
            CHOOSE[i:] = False
            break
            
    m_tot = np.min(m[CHOOSE])
    m_E = sbE[CHOOSE][np.argmin(m[CHOOSE])]
    if return_radius:
        tot_i = -1
        tru_i = -1
        loc = np.argmin(m[CHOOSE])
        for i in CHOOSE:
            tot_i += 1
            if i:
                tru_i += 1
            if tru_i == loc:
                break
        else:
            raise Exception('could not find argmin loc')
        return m_tot, m_E, tot_i
        
    else:
        return m_tot, m_E 

def modulate_n(n):
    return 3./(1+np.exp(-n)) + 1.

def BDmodel(R, SB, Re, Ie, n, band):
    
    # calculate the bulge light in the SB profile
    bulge_sb = ISB_to_muSB(sersic_I(np.array(R), Re, Ie, n), band)
    # calculate the remaining light that is assumed to be from the disk
    disk_sb = np.array(list(ISB_to_muSB(max(0.,s), band) for s in list(muSB_to_ISB(np.array(SB), band) - muSB_to_ISB(bulge_sb, band))))

    return ISB_to_muSB(muSB_to_ISB(bulge_sb, band) + muSB_to_ISB(disk_sb, band), band)

def BulgeDiskDecomposition(sb_R, sb_SB, band, bulge_radius = None):

    assert len(sb_R) > 5

    # Determine which set of points to use to fit the bulge
    results = []
    best = 0
    for i in range(1,10):
        res = minimize(lambda x: np.mean((BDmodel(sb_R, sb_SB, x[0], muSB_to_ISB(sb_SB[i], band) / sersic_I(sb_R[i], x[0], 1., modulate_n(x[1])), modulate_n(x[1]), band) - np.array(sb_SB))**2),
                       x0 = [sb_R[i]*4, 0.],
                       method = 'Nelder-Mead')
        results.append([res.fun, [res.x[0], muSB_to_ISB(sb_SB[i], band) / sersic_I(sb_R[i], res.x[0], 1., modulate_n(res.x[1])), modulate_n(res.x[1])]])
        if results[-1][0] <= results[best][0]:
            best = len(results) - 1

    # calculate the bulge light in the SB profile
    bulge_sb = ISB_to_muSB(sersic_I(np.array(sb_R), results[best][1][0], results[best][1][1], results[best][1][2]), band)
    # calculate the remaining light that is assumed to be from the disk
    disk_sb = list(ISB_to_muSB(max(0.,s), band) for s in list(muSB_to_ISB(np.array(sb_SB), band) - muSB_to_ISB(bulge_sb, band)))

    print(str(results[best][1][0]) + str(results[best][1][1]) + str(results[best][1][2]))
    
    return list(bulge_sb), list(disk_sb), [results[best][1][0], results[best][1][1], results[best][1][2]]

def ProfileCut(R, P, E, cut_value = None, n_fail = 3, n_cut = 5):
    """
    Checks that a profile's uncertainty values are consistent with the local scatter observed.
    It is physically not possible for the variation among points to be less than the uncertainty
    and so this code searches for a point where that occurs with some confidence.

    R: Radius values of profiles, sorted from lowest to highest
    P: Profiles values
    E: uncertainty in profiles values

    returns: index 
    """
    count_fail = 0
    count_cut = 0
    i_cut = len(R)

    for i in range(2, len(R)-2):
        if R[i] < 3:
            continue
        if np.std(P[i-2:i+3]) < 0.5 * np.sqrt(np.mean(E[i-2:i+3]**2)):
            count_fail += 1
        else:
            count_fail = 0
        if not cut_value is None and E[i] >= cut_value:
            count_cut += 1
        else:
            count_cut = 0
        if count_fail >= n_fail:
            i_cut = i-n_fail
            break
        if count_cut >= n_cut:
            i_cut = i - n_cut
            break
    else:
        i_cut = len(R) - count_cut
    return i_cut

import numpy as np
import os
import sys
sys.path.append(os.environ['PYTHON_GOODIES'])
from Smoothing import PolySmooth
sys.path.append(os.environ['PROGRAMMING'])
from CAstro.conversions import arcsec_to_pc, pc_to_arcsec, app_mag_to_abs_mag, mag_to_L, Grav_C, Pc_to_m, muSB_to_ISB
from scipy.integrate import quad
from scipy.optimize import minimize

# Sigma(R) stuff
######################################################################

# Sigma(R)
def SmoothSurfaceDensity(R, prof_R, prof_mu, M2L, band, n = 10):
    """
    R: Radius to evaluate the surface density (length units)
    prof_R: Radius values on the surface brightness profile (same length units)
    prof_mu: surface brightness values (mag arcsec^-2)
    M2L: Mass to light ratio (M_sun / L_sun; numpy array)
    band: photometric band in which surface brightness was measured
    n: number of points to take for extrapolation

    returns: Surface mass density (M_sun pc^-2)
    """

    if type(R) in [list, np.ndarray]:
        res = []
        for r in R:
            res.append(SmoothSurfaceDensity(r, prof_R, prof_mu, M2L, band, n = 10))
        print 'SmoothSurfaceDensity, ', res
        return np.array(res)
    
    if R < prof_R[0]:
        mu = np.polyval(np.polyfit(prof_R[:n], prof_mu[:n], deg = 1), R)
    elif R > prof_R[-1]:
        p = np.polyfit(prof_R[-n:], prof_mu[-n:], deg = 1)
        if p[0] < 0:
            print p[0]
            print prof_R[-n:], prof_mu[-n:]
            raise Exception('Outer profile rising, cannot extrapolate')
        mu = np.polyval(p, R)
    else:
        mu = PolySmooth(prof_R, prof_mu, R)

    return muSB_to_ISB(mu, band = band) * M2L[np.argmin(np.abs(R - prof_R))]

# dSigma(R) / dR
def dSmoothSurfaceDensity_dR(R, prof_R, prof_mu, M2L, band, n = 10):
    """
    R: Radius to evaluate the surface density (length units)
    prof_R: Radius values on the surface brightness profile (same length units)
    prof_mu: surface brightness values (mag arcsec^-2)
    M2L: Mass to light ratio (M_sun / L_sun; numpy array)
    band: photometric band in which surface brightness was measured
    n: number of points to take for extrapolation

    returns: Surface mass density (M_sun pc^-2)
    """

    if type(R) in [list, np.ndarray]:
        res = []
        for r in R:
            res.append(dSmoothSurfaceDensity_dR(r, prof_R, prof_mu, M2L, band, n))
        print 'dSmoothSurfaceDensity_dR, ', res
        return np.array(res)
    
    if R < prof_R[0]:
        dmudR = np.polyfit(prof_R[:n], prof_mu[:n], deg = 1)[0]
    elif R > prof_R[-1]:
        p = np.polyfit(prof_R[-n:], prof_mu[-n:], deg = 1)
        if p[0] < 0:
            print p[0]
            print prof_R[-n:], prof_mu[-n:]
            raise Exception('Outer profile rising, cannot extrapolate')
        dmudR = p[0]
    else:
        p = PolySmooth(prof_R, prof_mu, R, return_p = True, deg = 3)[0]
        
        dmudR = np.polyval((3. * p[0], 2. * p[1], p[2]), R)

    return - np.log(10.) * SmoothSurfaceDensity(R, prof_R, prof_mu, M2L, band, n) * dmudR / 2.5

# d^2Sigma(R) / dR^2
def d2SmoothSurfaceDensity_dR2(R, prof_R, prof_mu, M2L, band, n = 10):
    """
    R: Radius to evaluate the surface density (length units)
    prof_R: Radius values on the surface brightness profile (same length units)
    prof_mu: surface brightness values (mag arcsec^-2)
    M2L: Mass to light ratio (M_sun / L_sun; numpy array)
    band: photometric band in which surface brightness was measured
    n: number of points to take for extrapolation

    returns: Surface mass density (M_sun pc^-2)
    """

    if type(R) in [list, np.ndarray]:
        res = []
        for r in R:
            res.append(d2SmoothSurfaceDensity_dR2(r, prof_R, prof_mu, M2L, band, n))
        print 'd2SmoothSurfaceDensity_dR2, ', res
        return np.array(res)

    if R < prof_R[0]:
        d2mudR2 = 0.
    elif R > prof_R[-1]:
        d2mudR2 = 0.
    else:
        p = PolySmooth(prof_R, prof_mu, R, return_p = True, deg = 3)[0]
        d2mudR2 = 3. * 2. * p[0] * R + 2. * p[1]

    return SmoothSurfaceDensity(R, prof_R, prof_mu, M2L, band, n) * (np.log(10) / 2.5) * ((np.log(10) / 2.5) * dSmoothSurfaceDensity_dR(R, prof_R, prof_mu, M2L, band, n)**2 - d2mudR2)

# h(z) stuff
######################################################################

def hz(z, z0):
    return 1. / (2. * z0 * (np.cosh(z / z0)**2))

def Hz(z, z0):
    return z0 * (np.log(np.cosh(z / z0)) / 2. - z / (2. * z0) - np.log(2) / z0)

def dHzdz(z, z0):
    return np.tanh(z/z0)/2. - 1./2.

######################################################################

def rho_sech(R, z, z0, Sigma, args):
    """
    R: radius to evaluate the density
    z: height above plane of disk to evaluate density
    Sigma: Function giving the surface mass density (M_sun pc^-2)
    args: any arguments for Sigma

    returns: Mass density (M_sun pc^-3)
    """
    rho = Sigma(R, *args) / (2. * z0 * (np.cosh(z / z0)**2))

    return rho


def mu_param(R, z, z0, prof_R, prof_mu, M2L, band, n = 10):
    """
    R: radius about which to generate the parametrization
    z: height above plane of disk
    prof_R: Radius values on the surface brightness profile (same length units)
    prof_mu: surface brightness values (mag arcsec^-2)
    M2L: Mass to light ratio (M_sun / L_sun; numpy array)
    band: photometric band in which surface brightness was measured
    n: number of points to take for extrapolation/interpolation

    returns: list of polynomial coefficients, where p[0] is the x^3 term
    """
        
    if R < prof_R[1]:
        p = np.polyfit(prof_R[:n], prof_mu[:n], deg = 1)
        p = [0.,0.,p[0],p[1]]
    elif R > prof_R[-2]:
        p = np.polyfit(prof_R[-n:], prof_mu[-n:], deg = 1)
        if p[0] < 0:
            print p[0]
            print prof_R[-n:], prof_mu[-n:]
            raise Exception('Outer profile rising, cannot extrapolate')
        p = [0.,0.,p[0],p[1]]
    else:
        Nhalf = int(n/2)
        split = True
        if np.sum(prof_R < R) < Nhalf or np.sum(prof_R > R) < Nhalf:
            split = False
            
        if split:
            X_before = prof_R[prof_R < R]
            X_after  = prof_R[prof_R > R]
            N_before = np.argsort(np.abs(X_before - R))
            N_after  = np.argsort(np.abs(X_after - R))
            p = np.polyfit(np.concatenate((X_before[N_before[:Nhalf]], X_after[N_after[:Nhalf]])),
                           np.concatenate((prof_mu[prof_R < R][N_before[:Nhalf]], prof_mu[prof_R > R][N_after[:Nhalf]])),
                           deg = 3)
            
        else:
            N = np.argsort(np.abs(prof_R - R))
            p = np.polyfit(prof_R[N[:n_points]], prof_mu[N[:n_points]], deg = 3)

    return p
    
def dphifd_dR(R, z, z0, prof_R, prof_mu, M2L, band, n = 10):
    """
    R: radius to evaluate the density
    z: height above plane of disk to evaluate density
    prof_R: Radius values on the surface brightness profile (same length units)
    prof_mu: surface brightness values (mag arcsec^-2)
    M2L: Mass to light ratio (M_sun / L_sun; numpy array)
    band: photometric band in which surface brightness was measured
    n: number of points to take for extrapolation

    returns: derivative of the phi_fd potential wrt R
    """

    rho_Z = np.log(np.cosh(z / z0))

    C = 10**((21.571 + Abs_Mag_Sun[band])/2.5)
    rho_R = C * np.log(10) * 10**(-np.polyval(p,R) / 2.5) * (-3*p[0]*R**2 - 2*p[1]*R - p[2])
    
    return rho_R * rho_Z

def rho_res(R, z, z0, prof_R, prof_mu, M2L, band, n = 10):
    """
    R: radius to evaluate the density
    z: height above plane of disk to evaluate density
    prof_R: Radius values on the surface brightness profile (same length units)
    prof_mu: surface brightness values (mag arcsec^-2)
    M2L: Mass to light ratio (M_sun / L_sun; numpy array)
    band: photometric band in which surface brightness was measured
    n: number of points to take for extrapolation

    returns: - R^-1 (d/dR)(Rdphi_fd/dR)
    """

    rho_Z = np.log(np.cosh(z / z0))

    p = mu_param(R, z, z0, prof_R, prof_mu, M2L, band, n = 10)
    
    C = 10**((21.571 + Abs_Mag_Sun[band])/2.5)
    rho_R = -C * np.log(10) * 10**(-np.polyval(p,R) / 2.5) * (-R*np.log(10)*(3*p[0]*R**2 + 2*p[1]*R + p[2])**2 / 2.5**2 + 9 * p[0] * (R**2)/2.5 + 4*p[1]*R/2.5 + p[2]/2.5)

    return rho_R * rho_Z

def _MassUpTo(Rlim, prof_R, prof_mu, M2L, band, n):

    return 2. * np.pi * quad(lambda Ri: Ri * SmoothSurfaceDensity(Ri, prof_R, prof_mu, M2L, band, n), 0, Rlim)[0]

def SmoothSurfaceDensity_HalfMass(prof_R, prof_mu, M2L, band, n = 10):
    """
    Calculates the radius at which the profile reaches half mass
    """

    Mtot = _MassUpTo(np.inf, prof_R, prof_mu, M2L, band, n)

    Rhalf = minimize(lambda Ri: (Mtot/2. - _MassUpTo(Ri, prof_R, prof_mu, M2L, band, n))**2, [np.median(prof_R)/2.], method = 'Nelder-Mead').x[0]

    return Rhalf

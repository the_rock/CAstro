import numpy as np
from copy import copy
import matplotlib.pyplot as plt
from matplotlib.patches import Ellipse

def EllipseIntersect(E1, E2):
    """
    Determines if two ellipses intersect.

    E1, E2: Dictionary of information for each ellipse. includes
    "PA" position angle
    "a" semi-major axis length
    "b" semi-minor axis length, in the same units

    returns: boolean with true indicating that there is an intersection
    """

    # Variable for parametrized elipse
    t = np.linspace( 0, np.pi, 50)

    # Unrotated coordinates for first ellipse
    x = E1['a'] * np.cos(t)
    y = E1['b'] * np.sin(t)

    # Relative angle between ellipses
    theta = (E2['PA'] - E1['PA']) * np.pi / 180.0

    # Rotated coordinates for first ellipse
    xr, yr = (x * np.cos(theta) - y * np.sin(theta), x * np.sin(theta) + y * np.cos(theta))

    # Evaluating the second ellipse at the coordinates on the first
    sig = (xr**2) / E2['a']**2 + (yr**2) / E2['b']**2 - 1.0

    # Return true if there is an intersection
    if np.sum(sig > 0) == 0 or np.sum(sig < 0) == 0:
        return False
    else:
        return True

def ChooseEllipses(E, C = None):
    """
    Select a subset of ellipses that do not intersect. Recursively remove
    "most intersecting" ellipse until only non-intersecting one remain.

    E: List of ellipses, each ellipse/element is represented by a dictionary
    with "PA" position angle, "a" semi-major-axis, "b" semi-minor axis
    C: list used for recursive evaluation.

    returns: indices of ellipses that are non-intersecting
    """

    # Don't work with a small number of ellipses
    E = np.array(E)
    if len(E) < 3:
        raise Exception('Not enough elipses!')

    # Counter for each ellipse to track the number of intersections it has
    counts = np.zeros(len(E))

    # Itterate through every ellipse
    for i in range(len(E)-1):
        # Skip ellipses which have already been found not to intersect
        if C is not None and C[i] == 0:
            continue
        # Itterate through every other ellipse to make pairs
        for j in range(i+1,len(E)):
            # Skip ellipses which have already been found not to intersect
            if C is not None and C[j] == 0:
                continue
            # If they intersect, add to both of their counters
            if EllipseIntersect(E[i], E[j]):
                counts[i] += 1
                counts[j] += 1

    # If there are no intersections, return an array that indexes all ellipses (accept them all)
    if np.sum(counts > 0) == 0:
        return np.array(range(len(E)),dtype=int)
    # If several isophotes have intersections, recursively apply the ellipse chooser, dropping the most offending isophote
    else:
        # Sort the ellipses by number of intersections
        N = np.argsort(-counts)
        # Recursively apply the ellipse chooser, droping the most intersecting ellipse
        new_E = ChooseEllipses(E[N[1:]], counts[N[1:]])
        return N[1:][new_E]
            

def EllipseDraw(a, b, PA, name = '', intersect = True, skip = 5):
    """
    Draw a series of ellipses. Can be instructed to determine
    if ellipses are intersecting, and return a list of
    non-intersecting elements.

    a: semi-major axis list
    b: semi-minor axis list [same unit as a]
    PA: position angle list [degrees]
    name: string to include in the file name, also the name of the ellipse for the saved file
    intersect: boolean to compute intersecting ellipses or not
    skip: skip ellipses, only plotting every N'th ellipse

    returns: N list of non-intersecting indices, only if intersect = True
    """
    print len(a)
    print len(b)
    print len(PA)
    assert len(a) == len(b)
    assert len(a) == len(PA)

    E = {}
    if intersect:
        pre_calc = False
        try:
            with open('Ellipses.txt', 'r') as f:
                E = eval(f.read())
            if name in E:
                N = np.array(E[name])
                pre_calc = True
                print 'using precalculated ellipses'
        except:
            pass
        
        if not pre_calc:
            N = ChooseEllipses(list({'a':x, 'b':y, 'PA':z} for x,y,z in zip(a,b,PA)))
    else:
        N = range(len(a))
        
    plt.clf()
    f, ax = plt.subplots()

    for i in range(len(a)):
        if i % skip == 0:
            ax.add_artist(Ellipse([0,0], 2*a[i], 2*b[i], PA[i], fill = False, linewidth = 0.1, edgecolor = 'r' if i in N else 'b'))

    ax.set_ylim([-max(a), max(a)])
    ax.set_xlim([-max(a), max(a)])
    ax.set_axis_off()
    plt.savefig('ellipse_%s.pdf' % name)#, transparent = True
    plt.close()
    plt.clf()

    if intersect:
        E[name] = list(N)
        with open('Ellipses.txt', 'w') as f:
            f.write(str(E))

        return np.sort(N)

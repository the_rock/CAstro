import matplotlib.pyplot as plt
import numpy as np

def RC_plot(R, RC, close = True, add_to_axis = None, share_x_axis = None, message = '', saveas = None, fixlims = True, xunit = 'arcsec', colour = 'r'):
    """
    Plots a rotation curve
    """

    assert len(R) == len(RC)
    
    if add_to_axis is None and share_x_axis is None:
        f, ax = plt.subplots()
    elif not add_to_axis is None:
        f, ax = add_to_axis
    elif not share_x_axis is None:
        f = share_x_axis[0]
        ax = share_x_axis[1].twinx()

    ax.scatter(R, RC, label = 'Rotation Curve' if message == '' else message, c = colour)
    ax.set_xlabel('Radius (%s)' % xunit)
    ax.set_ylabel('Velocity (km s${}^{-1}$)')
    if fixlims:
        ax.set_xlim([0,ax.get_xlim()[1]*1.05])
        ax.set_ylim([0,ax.get_ylim()[1]*1.05])

    if not saveas is None:
        plt.savefig(saveas)

    if close:
        plt.close(f)

    return (f,ax)


import numpy as np
import sys
import os
sys.path.append(os.environ['CAstro'])
from conversions import ISB_to_muSB, muSB_to_ISB, Abs_Mag_Sun, halfmag, magperarcsec2_to_mag, mag_to_magperarcsec2, mag_to_L, L_to_mag, arcsec_to_pc, app_mag_to_abs_mag, abs_mag_to_app_mag
from scipy.integrate import trapz
import matplotlib.pyplot as plt

def SBprof_to_COG(R, SB, axisratio, method = 0):
    """
    Converts a surface brightness profile to a curve of growth by integrating
    the SB profile in flux units then converting back to mag units. Two methods
    are implemented, one using the trapezoid method and one assuming constant
    SB between isophotes. Trapezoid method is in principle more accurate, but
    may become unstable with erratic data.

    R: Radius in arcsec
    SB: surface brightness in mag arcsec^-2
    axisratio: b/a indicating degree of isophote ellipticity
    method: 0 for trapezoid, 1 for constant

    returns: magnitude values at each radius of the profile in mag
    """
    
    m = np.zeros(len(R))
    # Dummy band for conversions, cancelled out on return
    band = 'r'

    # Method 0 uses trapezoid method to integrate in flux space
    if method == 0:
        # Compute the starting point assuming constant SB within first isophote
        m[0] = magperarcsec2_to_mag(SB[0], A = np.pi*axisratio[0]*(R[0]**2))
        # Dummy distance value for integral
        D = 10
        # Convert to flux space
        I = muSB_to_ISB(np.array(SB), band)
        # Ensure numpy array
        axisratio = np.array(axisratio)
        # Convert to physical radius using dummy distance
        R = arcsec_to_pc(np.array(R), D)
        # Integrate up to each radius in the profile
        for i in range(1,len(R)):
            m[i] = abs_mag_to_app_mag(L_to_mag(trapz(2*np.pi*I[:i+1]*R[:i+1]*axisratio[:i+1],R[:i+1]) + \
                                               mag_to_L(app_mag_to_abs_mag(m[0], D), band), band),D)
    elif method == 1:
        # Compute the starting point assuming constant SB within first isophote
        m[0] = magperarcsec2_to_mag(SB[0], A = np.pi*axisratio[0]*(R[0]**2))
        # Progress through each radius and add the contribution from each isophote individually
        for i in range(1,len(R)):
            m[i] = L_to_mag(mag_to_L(magperarcsec2_to_mag(SB[i], A = np.pi*axisratio[i]*(R[i]**2)), band) - \
                            mag_to_L(magperarcsec2_to_mag(SB[i], A = np.pi*axisratio[i-1]*(R[i-1]**2)), band) + \
                            mag_to_L(m[i-1], band), band)

    return m


def SBprof_to_ExtrapTotalMag(R, SB, SB_E, axisratio):
    """
    Converts a surface brightness profile to a curve of growth by integrating
    the SB profile in flux units then converting back to mag units. Two methods
    are implemented, one using the trapezoid method and one assuming constant
    SB between isophotes. Trapezoid method is in principle more accurate, but
    may become unstable with erratic data.

    R: Radius in arcsec
    SB: surface brightness in mag arcsec^-2
    axisratio: b/a indicating degree of isophote ellipticity
    method: 0 for trapezoid, 1 for constant

    returns: magnitude values at each radius of the profile in mag
    """
    
    # Dummy band for conversions, cancelled out on return
    band = 'r'

    CHOOSE = np.logical_and(np.array(SB) > 0, np.array(SB) < 25)
    CHOOSE = np.logical_and(CHOOSE, np.array(SB_E) < 0.3)
    # Compute the starting point assuming constant SB within first isophote
    m0 = magperarcsec2_to_mag(SB[0], A = np.pi*axisratio[0]*(R[0]**2))
    # Dummy distance value for integral
    D = 10
    # Convert to flux space
    I = muSB_to_ISB(np.array(SB)[CHOOSE], band)
    # Ensure numpy array
    axisratio = np.array(axisratio)[CHOOSE]
    # Convert to physical radius using dummy distance
    R = arcsec_to_pc(np.array(R)[CHOOSE], D)

    # Extrapolate SB
    p = np.polyfit(np.array(R)[R > (R[-1]/2)], np.array(SB)[CHOOSE][R > (R[-1]/2)], 1)
    if p[0] <= 0:
        print('SB increasing with radius!')
        p = np.polyfit(np.array(R)[R > (3*R[-1]/4)], np.array(SB)[CHOOSE][R > (3*R[-1]/4)], 1)
        if p[0] <= 0:
            print('cannot correct')
            # plt.errorbar(np.array(R), np.array(SB)[CHOOSE], yerr = np.array(SB_E)[CHOOSE], linewidth = 1, marker = 'o', color = 'k')
            # plt.plot(np.array(R)*1.2, np.polyval(p, np.array(R)*1.2), linestyle = '--', color = 'b')
            # plt.xlabel('R')
            # plt.ylabel('SB')
            # plt.savefig('deletemetestplot_%i.png' % np.random.randint(10000))
            # plt.clf()
            return np.inf
    scatter = np.sqrt(np.mean((np.polyval(p, np.array(R)[R > (R[-1]/2)]) - np.array(SB)[CHOOSE][R > (R[-1]/2)])**2)) / np.sqrt(len(np.array(R)[R > (R[-1]/2)]))
    Rbeyond = np.linspace(R[-1]*1.01, R[-1]*10, 10*len(R))
    SBbeyond = np.polyval(p, Rbeyond)
    Ibeyond = muSB_to_ISB(SBbeyond, band)
    axisratiobeyond = np.ones(len(Rbeyond))*np.median(axisratio[-5:])
    # Integrate up to each radius in the profile
    L_extrap = trapz(2*np.pi*np.concatenate((I,Ibeyond))*np.concatenate((R,Rbeyond))*np.concatenate((axisratio,axisratiobeyond)),
                     np.concatenate((R,Rbeyond)))
    m_extrap = abs_mag_to_app_mag(L_to_mag(L_extrap + mag_to_L(app_mag_to_abs_mag(m0, D), band), band),D)

    L_ERR = trapz(2*np.pi*np.concatenate((muSB_to_ISB(np.array(SB)[CHOOSE] - scatter, band),muSB_to_ISB(SBbeyond - scatter, band)))*np.concatenate((R,Rbeyond))*np.concatenate((axisratio,axisratiobeyond)),
                  np.concatenate((R,Rbeyond)))
    m_ERR = abs_mag_to_app_mag(L_to_mag(L_ERR + mag_to_L(app_mag_to_abs_mag(m0, D), band), band),D)
    
    return m_extrap, np.abs(m_ERR - m_extrap)

def SBprof_to_COG_errorprop(R, SB, SBE, axisratio, axisratioE = None, N = 100, method = 0, symmetric_error = True):
    """
    Converts a surface brightness profile to a curve of growth by integrating
    the SB profile in flux units then converting back to mag units. Two methods
    are implemented, one using the trapezoid method and one assuming constant
    SB between isophotes. Trapezoid method is in principle more accurate, but
    may become unstable with erratic data. An uncertainty profile is also
    computed, from a given SB uncertainty profile and optional axisratio
    uncertainty profile.
    
    R: Radius in arcsec
    SB: surface brightness in mag arcsec^-2
    SBE: surface brightness uncertainty relative mag arcsec^-2
    axisratio: b/a indicating degree of isophote ellipticity
    axisratioE: uncertainty in b/a
    N: number of iterations for computing uncertainty
    method: 0 for trapezoid, 1 for constant
    
    returns: magnitude and uncertainty profile in mag
    """

    # If not provided, axis ratio error is assumed to be zero
    if axisratioE is None:
        axisratioE = np.zeros(len(R))
        
    # Create container for the monte-carlo iterations
    COG_results = np.zeros((N, len(R))) + 99.999
    SB_CHOOSE = np.logical_and(np.isfinite(SB), SB < 50)
    if np.sum(SB_CHOOSE) < 5:
        return (None, None) if symmetric_error else (None, None, None)
    COG_results[0][SB_CHOOSE] = SBprof_to_COG(R[SB_CHOOSE], SB[SB_CHOOSE], axisratio[SB_CHOOSE], method = method)
    for i in range(1,N):
        # Randomly sampled SB profile
        tempSB = np.random.normal(loc = SB, scale = SBE)
        # Randomly sampled axis ratio profile
        tempq = np.random.normal(loc = axisratio, scale = axisratioE)
        # Compute COG with sampled data
        COG_results[i][SB_CHOOSE] = SBprof_to_COG(R[SB_CHOOSE], tempSB[SB_CHOOSE], tempq[SB_CHOOSE], method = method)

    # Condense monte-carlo evaluations into profile and uncertainty envelope
    COG_profile = COG_results[0]
    COG_lower = np.median(COG_results, axis = 0) - np.quantile(COG_results, 0.317310507863/2, axis = 0)
    COG_upper = np.quantile(COG_results, 1. - 0.317310507863/2, axis = 0) - np.median(COG_results, axis = 0)

    # Return requested uncertainty format
    if symmetric_error:
        return COG_profile, np.abs(COG_lower + COG_upper)/2
    else:
        return COG_profile, COG_lower, COG_upper
    

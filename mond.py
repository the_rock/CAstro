import numpy as np
from scipy.optimize import minimize

def mond_gobs(theta, g_bar):
    """
    Computes the observed acceleration given the prediceted baryonic acceleration.
    
    theta: parametrization for the RAR, g_dagger from Lelli et al. 2017 (m s^-2)
    g_bar: the baryonic acceleration (m s^-2)

    returns: the observed accelleration from the rotation curve (m s^-2)
    """

    try:
        return np.log10(g_bar / (1. - np.exp( - np.sqrt(g_bar / theta[0]))))
    except:
        return np.log10(g_bar / (1. - np.exp( - np.sqrt(g_bar / theta))))

def mond_gobs_log(theta, g_bar):
    """
    Computes the observed acceleration given the prediceted baryonic acceleration.
    
    theta: parametrization for the RAR, g_dagger from Lelli et al. 2017 (log10(g_dagger / m s^-2))
    g_bar: the baryonic acceleration (log10(g_bar / m s^-2))

    returns: the observed accelleration from the rotation curve (log10(g_obs / m s^-2))
    """
    try:
        return np.log10(10**g_bar / (1. - np.exp( - 10**((g_bar - theta[0])/2.))))
    except:
        return np.log10(10**g_bar / (1. - np.exp( - 10**((g_bar - theta)/2.))))

def mond_gbar_log(theta, g_obs):
    """
    Computes the observed acceleration given the prediceted baryonic acceleration.
    
    theta: parametrization for the RAR, g_dagger from Lelli et al. 2017 (log10(g_dagger/m s^-2))
    g_obs: the observed acceleration (m s^-2)

    returns: the baryonic accelleration from the light profile (log10(g_bar/m s^-2))
    """

    try:
        R = np.zeros(len(g_obs))
        for i in range(len(g_obs)):
            R[i] = minimize(lambda gb: (g_obs[i] - mond_gobs_log(theta,gb))**2, x0 = [g_obs[i]], method = 'Nelder-Mead').x[0]
        return R
    except:
        return minimize(lambda gb: (g_obs - mond_gobs_log(theta,gb))**2, x0 = [g_obs], method = 'Nelder-Mead')
            

def mond(theta, g_bar,g_obs):
    """
    Compares the measured observed acceleration and the measured baryonic acceleration
    through the RAR relation from Lelli et al. 2017. The comparison is done in log space.

    theta: parametrization for the RAR, g_dagger from Lelli et al. 2017 (m s^-2)
    g_bar: the baryonic acceleration (m s^-2)
    g_obs: the observed acceleration (m s^-2)

    returns: (log10(gobs) - log10(mond_model_gobs(gbar)))^2 comparison of the predicted and measured g_obs
    """

    try:
        return np.mean((np.log10(g_obs / 10**mond_gobs(theta[0], g_bar)))**2)
    except:
        return np.mean((np.log10(g_obs / 10**mond_gobs(theta, g_bar)))**2)

def mond_least_distance(theta, g_bar, g_obs):
    """
    Finds the point on the RAR that is closest to the given g_bar,g_obs location

    theta: parametrization for the RAR, g_dagger from Lelli et al. 2017 (m s^-2)
    g_bar: the baryonic acceleration (m s^-2)
    g_obs: the observed acceleration (m s^-2)

    returns: [g_bar, g_obs] closest point on the RAR
    """

    def dist(x):
        return (x[0] - g_bar)**2 + (mond_gobs_log(theta, x[0]) - g_obs)**2

    res = minimize(dist, x0 = [g_bar], method = 'Nelder-Mead')

    return res.x[0], mond_gobs_log(theta, res.x[0])

def double_power_law_log(theta, g_bar):

    """
    Computes the observed acceleration given the prediceted baryonic acceleration.
    
    theta: parametrization for the double power law, from Lelli et al. 2017
           theta[0] = \hat{y}
           theta[1] = \hat{x}
           theta[2] = \hat{\alpha}
           theta[3] = \hat{\beta}
    g_bar: the baryonic acceleration in logarithmic scale (log10(g_bar / m s^-2))

    returns: the observed accelleration from the rotation curve in logarithmic scale (log10(g_obs / m s^-2))
    """
    return np.log10(theta[0] * ((1. + ((10**(g_bar+10)) / theta[1]))**(theta[2] - theta[3])) * (((10**(g_bar+10)) / theta[1])**theta[3])) - 10.

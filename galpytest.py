import numpy as np
import galpy
from galpy.potential import scf_compute_coeffs_axi, SCFPotential
from Density import rho_sech
from galpy.potential import TriaxialNFWPotential
import matplotlib.pyplot as plt
# np = TriaxialNFWPotential(normalize=1.,c=1.4,a=1.)

def Sigma(R):
    return 50. * np.exp(- R / 5000.)
def rho(R,z, xtra):
    print R, z, xtra
    return rho_sech(R, z, 300., Sigma, tuple())

Acos, Asin = scf_compute_coeffs_axi(rho, 80, 40, 5000.)
print '*'*50
print Acos

sp = SCFPotential(Acos=Acos,Asin=Asin,a=5000.)
print type(sp)
xs = np.linspace(0.,300.,1001)
plt.scatter(xs,sp.dens(xs,xs))
# plt.yscale('log')
# plt.xscale('log')
plt.savefig('galpy_test.png')
plt.clf()

sp.plotRotcurve(savefilename = 'galpy_rotcurve_test.png')

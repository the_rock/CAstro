import numpy as np
  
def inclination(axisratio, axisratio_min):
    """
    axisratio: the measured axis ratio (b/a) (unitless)
    axisratio_min: the assumed minimum axis ratio (b_min/a) (unitless)

    returns: corrected inclination of galaxy (radians)
    """

    axisratio = np.reshape(np.array(axisratio),-1)
    axisratio[axisratio < 0] = 1e-3
    axisratio[axisratio > 1] = 1 - 1e-3
    assert np.all(np.logical_and(0 <= axisratio, axisratio <= 1))
    assert np.all(np.logical_and(0 <= axisratio_min, axisratio_min < 1))

    if type(axisratio) in [float,np.float64]:
        axisratio = np.array([axisratio])
        
    ret = np.arccos(np.sqrt((axisratio**2 - axisratio_min**2)/(1. - axisratio_min**2)))
    if type(ret) is np.ndarray:
        ret[axisratio < axisratio_min] = np.pi/2.
    elif not np.isfinite(ret):
        ret = np.pi/2.

    return ret if len(ret) > 1 else ret[0]

    
def axisratio(i, axisratio_min):
    """
    i: corrected inclination of galaxy (radians)
    axisratio_min: the assumed minimum axis ratio when the corrected inclination was calculated (b_min/a) (unitless)

    returns: the measured axis ratio (b/a) (unitless)
    """
    if axisratio_min < 0:
        axisratio_min = 1e-3
    elif axisratio_min > 1:
        axisratio_min = 1 - 1e-3
    #assert np.all(np.logical_and(0 <= axisratio_min, axisratio_min < 1))
    assert np.all(np.logical_and(0 <= i,  i <= np.pi/2))

    if i >= np.pi/2. - 1e-3:
        return axisratio_min
    else:
        return np.sqrt((np.cos(i)**2) * (1. - axisratio_min**2) + axisratio_min**2)

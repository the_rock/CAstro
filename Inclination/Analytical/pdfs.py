import numpy as np
from scipy.stats import truncnorm


# Axis Ratio
######################################################################
def axisratio_min_sample(N):
    assert type(N) == int and N > 0

    return (truncnorm.rvs(a = -3, b = 3, size = N) * 0.2 / 3.) + 0.2

def axisratio_min_pdf(x):
    assert np.all(np.logical_and(0 <= x, x <= 1))

    if type(x) == float:
        x = np.array([x])

    ret = np.zeros(len(x))
    ret[x < 0.2] = 25. * x[x < 0.2]
    ret[np.logical_and(x >= 0.2, x < 0.4)] = 10. - 25. * x[np.logical_and(x >= 0.2, x < 0.4)]
    return ret if len(ret) > 1 else ret[0]

def axisratio_min_cdf(x):
    assert np.all(np.logical_and(0 <= x, x <= 1))

    if type(x) == float:
        x = np.array([x])

    ret = np.ones(len(x))
    ret[x < 0.2] = 25. * x[x < 0.2]**2 / 2.
    ret[np.logical_and(x >= 0.2, x < 0.4)] = 1. - 25. * (0.4 - x[np.logical_and(x >= 0.2, x < 0.4)])**2 / 2.
    return ret if len(ret) > 1 else ret[0]
    
def axisratio_min_icdf(y):
    assert np.all(np.logical_and(0 <= y, y <= 1))

    if type(y) == float:
        y = np.array([y])
    ret = np.zeros(len(y))
    ret[y < 0.5] = np.sqrt(2. * y[y < 0.5] / 25.)
    ret[y >= 0.5] = 0.4 - np.sqrt(2. * (1. - y[y >= 0.5])) / 5.
    return ret if len(ret) > 1 else ret[0]
    
    

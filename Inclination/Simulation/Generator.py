import numpy as np

def GenerateGalaxy(Rd, Hz, axisratio = 1, N = int(1e4)):
    """
    Rd: Scale length of galaxy
    Hz: Scale Height of galaxy
    axisratio: The intrinsic axis ratio of galaxy
    N: Number of particles to generate
    """
    assert Rd > 0
    assert Hz >= 0
    assert 0 < axisratio <= 1
    assert type(N) == int and N > 0

    # The particles are uniformly distributed in angle
    theta = np.random.rand(N) * 2 * np.pi

    # The particles are exponentially distributed in radius.
    # The distribution can be made non-circular (ellipsoidal) if desired
    R = - (Rd/2.) * ((1. - axisratio)*np.cos(2*theta) + 1. + axisratio) * np.log(1-np.random.rand(N))

    # The particles are expoentially distributed in height above the galaxy plane.
    Z = - Hz * np.log(1-np.random.rand(N)) * np.random.choice([1., -1.], N)

    return {'coordinates': 'cylindrical', 'theta': theta, 'R': R, 'Z': Z}


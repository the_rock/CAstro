import numpy as np
import matplotlib.pyplot as plt

def Collapse(Cor):

    if Cor['coordinates'] == 'cylindrical':
        raise ValueError('Should be in Cartesian coordinates')

    print min(Cor['Y'])

    hist, bins = np.histogram(Cor['Y'], bins = 100)
    plt.bar(bins[:-1], hist, width = bins[1] - bins[0])
    plt.show()
    hist, binsx, binsy = np.histogram2d(Cor['Y'], Cor['Z'], bins = int(np.sqrt(len(Cor['Y']))))
    print binsx[0]
    print binsy[0]

    plt.imshow(np.log(hist.T), origin = 'lower', extent = [binsx[0],binsx[-1],binsy[0],binsy[-1]])#, extent = [binsx[0],binsx[-1],binsy[0],binsy[-1]]
    plt.show()

from Generator import GenerateGalaxy
from Manipulate import Cylindrical2Cartesian, Cartesian2Cylindrical, Incline
G = GenerateGalaxy(3., 0.1)
G1 = Incline(Cylindrical2Cartesian(G), np.pi/2)
G2 = Cartesian2Cylindrical(G1)
print 'G', min(G['theta']), max(G['theta'])
#print 'G1', min(G1['theta']), max(G1['theta'])
print 'G2', min(G2['theta']), max(G2['theta'])
print 'G1', min(G1['Y']), max(G1['Y'])
Collapse(G1)

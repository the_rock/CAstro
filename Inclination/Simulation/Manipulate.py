import numpy as np


def Cylindrical2Cartesian(Cyl):
    """
    Convert a coordinates dictionary from Cylindrical coordinates
    to Cartesian coordinates.
    """

    # Store cartesian coordinates in a dictionary
    Car = {'coordinates': 'cartesian',
           'X': Cyl['R'] * np.cos(Cyl['theta']),
           'Y': Cyl['R'] * np.sin(Cyl['theta']),
           'Z': Cyl['Z']}

    return Car

def Cartesian2Cylindrical(Car):
    """
    Convert a coordinates dictionary from Cartesian coordinates
    to Cylindrical coordinates.
    """
    
    # Store cylindrical coordinates in a dictionary
    Cyl = {'coordinates': 'cylindrical',
           'R': np.sqrt(Car['X']**2 + Car['Y']**2),
           'theta': np.arctan(np.abs(Car['Y'] / Car['X'])),
           'Z': Car['Z']}
    # Correct the arctan angles to all be from the x axis
    Cyl['theta'][np.logical_and(Car['X'] < 0, Car['Y'] < 0)] += np.pi
    Cyl['theta'][np.logical_and(Car['X'] < 0, Car['Y'] > 0)] = np.pi - Cyl['theta'][np.logical_and(Car['X'] < 0, Car['Y'] > 0)]
    Cyl['theta'][np.logical_and(Car['X'] > 0, Car['Y'] < 0)] += 2*np.pi - Cyl['theta'][np.logical_and(Car['X'] > 0, Car['Y'] < 0)]    

    return Cyl


def Spin(C, angle):
    """
    Spin the galaxy about its z axis. To be used before inclining
    the galaxy. This only matters when there is some intrinsic
    ellipticity to the galaxy, as it will orrient the ellipticity
    off from the viewing direction. This accounts for the random
    directions that galaxies are aligned.
    """
    
    assert type(C) == dict
    assert 'coordinates' in C
    assert 0 <= angle <= 2 * np.pi

    if C['coordinates'] == 'cylindrical':
        D = {'coordinates': 'cylindrical',
             'R': C['R'],
             'Z': C['Z']}
        D['theta'] = (C['theta'] + angle) % (2*np.pi)
    elif C['coordinates'] == 'cartesian':
        D = Cylindrical2Cartesian(Spin(Cartesian2Cylindrical(C), angle))
    else:
        raise ValueError('Given coordinates must state system (cartesian or cylindrical)')

    return D


def Incline(C, angle):
    """
    Incline the galaxy relative to the viewing direction. The viewer is
    considered to be on the x-axis, the galaxy coordinates are modified
    accordingly. This accounts for the random orientation on the sky.
    """

    assert type(C) == dict
    assert 'coordinates' in C
    assert 0 <= angle <= np.pi/2

    if C['coordinates'] == 'cartesian':
        D = {'coordinates': 'cartesian',
             'Y': C['Y']}
        D['X'] = C['X'] * np.cos(angle) - C['Z'] * np.sin(angle)
        D['Z'] = C['X'] * np.sin(angle) + C['Z'] * np.cos(angle)
    elif C['coordinates'] == 'cylindrical':
        D = Cartesian2Cylindrical(Incline(Cylindrical2Cartesian(C),angle))
    else:
        raise ValueError('Given coordinates must state system (cartesian or cylindrical)')

    return D

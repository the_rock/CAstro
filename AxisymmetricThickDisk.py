import numpy as np
from scipy.integrate import quad as solver
from scipy.special import ellipk, ellipe, i0, k0, i1, k1
import matplotlib.pyplot as plt
from conversions import mag_to_L


G = 4.302e-6 # kpc M_sun^-1 (km s^-1)^2


def SampleMass_to_N(R, Mass, Z0, N):
    """
    R: radius (kpc)
    Mass: Mass at a given radius R (M_sun)
    N: number of samples
    """

    assert np.all(Mass[:1] < Mass[1:])

    if not (R[0] == 0 and Mass[0] == 0):
        R = np.concatenate(([0], R))
        Mass = np.concatenate(([0], Mass))

    m = (Mass[1:] - Mass[:1]) / (R[1:] - R[:1]) # (y2 - y1) / (x2 - x1)

    sample_M = np.random.rand(N) * Mass[-1]

    index = []
    for i in range(N):
        for x in range(len(Mass)):
            if Mass[x] > sample_M[i]:
                index.append(x-1)
                break
    index = np.array(index)
    sample_R = R[index] + (sample_M - Mass[index]) / m[index]

    sample_theta = np.random.rand(N) * 2 * np.pi

    sample_Z = - np.log(1 - np.random.rand(N)) * Z0 * np.random.choice([-1,1], N)
    

    # sample_r = np.sqrt(sample_R**2 + sample_Z**2)

    # sample_phi = np.arctan(sample_Z / sample_R)

    return {'coordinates': zip(list(sample_R), list(sample_theta), list(sample_Z)), 'ParticleMass': Mass[-1] / N}

def DescreteMassPotential(R, Theta, Z, M, x):
    """
    R: cylindrical radius of particles (kpc)
    Theta: cylindrical angle of particles (radians)
    Z: cylindrical z axis (kpc)
    M: mass of particles (M_sun)
    x: locations to evaluate the potential (kpc)
    """

    X = R * np.cos(Theta)
    Y = R * np.sin(Theta)

    phi = []
    
    for i in range(len(x)):

        phi.append( - G * np.sum(M / np.sqrt((X - x[i])**2 + Y**2 + Z**2 + 1e-2)))

    return np.array(phi)

def DescreteMassVelocityProfile(R, Theta, Z, M, x):

    X = R * np.cos(Theta)
    Y = R * np.sin(Theta)

    V = []

    for i in range(len(x)):

        V.append( np.sqrt(np.abs(x[i] * G * np.sum(M * (X - x[i]) / ((X - x[i])**2 + Y**2 + Z**2 + 1e-1)**(3./2.)))) )

    return np.array(V)



def ExponentialSurfaceDensity(Sigma0, Rd, N):

    Sample_M = np.random.rand(N) * 2 * np.pi * Sigma0 * Rd**2

    R = np.logspace(-2, np.log10(Rd) + 2, 1000)

    Mr = 2 * np.pi * Sigma0 * (Rd**2 - Rd * (Rd + R) * np.exp( - R / Rd))

    if not (R[0] == 0 and Mass[0] == 0):
        R = np.concatenate(([0], R))
        Mr = np.concatenate(([0], Mr))

    m = (Mr[1:] - Mr[:1]) / (R[1:] - R[:1]) # (y2 - y1) / (x2 - x1)

    index = []
    for i in range(N):
        for x in range(len(Mr)):
            if Mr[x] > Sample_M[i]:
                index.append(x-1)
                break
            if x == len(Mr) - 1:
                print 'problem!!!'
    index = np.array(index)
    sample_R = R[index] + (Sample_M - Mr[index]) / m[index]

    sample_theta = np.random.rand(N) * 2 * np.pi

    ParticleMass = 2 * np.pi * Sigma0 * Rd**2 / N

    X = sample_R * np.cos(sample_theta)
    Y = sample_R * np.sin(sample_theta)

    #plt.scatter(X, Y, s = 0.1)
    hist, binsx, binsy = np.histogram2d(X,Y,bins = [200,200], range = [[-60,60],[-60,60]])
    hist[hist == 0] = np.nan
    plt.imshow(hist.T, extent = [binsx[0],binsx[-1],binsy[0],binsy[-1]], origin = 'lower')
    plt.savefig('exponentialsample.png')
    plt.clf()

    V = DescreteMassVelocityProfile(sample_R, sample_theta, 0.0, ParticleMass, np.linspace(0,100,100))
    plt.scatter(np.linspace(0,100,100), V, c = 'b', label = 'Monte-Carlo')
    y = np.linspace(0,100,100) / (2 * Rd)
    prt1 = 4 * np.pi * G  * Sigma0 * Rd
    prt2 = y**2
    prt3 = (i0(y) * k0(y) - i1(y) * k1(y))
    plt.scatter(np.linspace(0,100,100), np.sqrt(prt1 * prt2 * prt3), c = 'r', label = 'Theoretical')
    plt.xlabel('Radius (kpc)')
    plt.ylabel('Velocity (km s^-1)')
    plt.legend()
    plt.savefig('exponentialdistributionrotationcurve.png')
    plt.clf()
    print np.sqrt(prt1 * prt2[-1] * prt3[-1])
    print V[-1]
    print np.sqrt(prt1 * prt2[-1] * prt3[-1]) / V[-1]

    with open('ExponentialProfile.txt', 'w') as f:
        f.write('Sigma_0: %.2e (M_sun kpc^-2), R_d: %.2f (kpc)\n' % (Sigma0, Rd))
        f.write('M (M_sun)  X (kpc)  Y (kpc)  Z (kpc)\n')
        for i in range(N):
            f.write('%.3f %.3f %.3f %.3f\n' % (ParticleMass, X[i], Y[i], 0.))

# sigma0 = 100. * 1e6
# rd = 5.

# ExponentialSurfaceDensity(sigma0, rd, int(1e5))



# def int_dzeta(z, u, R, drho, args):
#     """
#     integrand over the variable zeta
#     """

#     x = (R**2 + u**2 + z**2) / (2. * R * u)

#     p = x - np.sqrt(x**2 - 1)

#     print (ellipk(p) - ellipe(p))
    
#     return 2. * np.sqrt(u / (R * p)) * (ellipk(p) - ellipe(p)) * drho(R, z, *args) / np.pi

# def int_du(u, R, drho, args):
#     """
#     integrand over the variable u
#     """

#     return solver(int_dzeta, 0., np.inf, args = (u, R, drho, args))[0]

# def Fr(R, drho, args):
#     """
#     Computes the radial force in the galactic plane for an axially symmetric density distribution.
#     See Casertano 1983
    
#     R: The radius at which to evaluate the force
#     drho: The partial derivative of the density function rho(R,z) in the R direction.
#     args: extra arguments to drho

#     returns: F_r the force in the radial direction (not actually a force)
#     """

#     res = []
#     for Ri in R:
#         temp = solver(int_du, 0., np.inf, args = (Ri, drho, args))
#         print temp[1], temp[1] / temp[0]
#         if np.isfinite(temp[0]) and np.abs(temp[1] / temp[0]) < 1e-1 and temp[1] < 1e10:
#             res.append(4. * np.pi * G * temp[0])
#         else:
#             res.append(np.nan)
            
#     return np.array(res)

# def sech_drho(R, z, z0, dSigma, args):
#     """
#     density profile from Dutton et al. 2005 (equation from van der Kruit & Searle 1981)

#     R: radius at which to evaluate the density derivative (cylindrical coordinates)
#     z: heigh above the disk to evaluate the density derivative (cylindrical coordinates)
#     z0: vertical scale height z0 = q0 * Rd, where q0 is the intrinsic thickness and Rd is the disk scale length
#     dSigma: partial derivative of the surface density profile wrt R
#     args: arguments for dSigma

#     returns: rho(R,z) density evaluated at specified point
#     """

#     return dSigma(R, *args) / (2. * z0 * (np.cosh(z / z0)**2))


# def exponential_dSigma(R, Sigma0, Rd):
#     """
#     derivative of the surface density for an exponential surface density profile wrt R.

#     R: radius to evaluate the surface density derivative (cylindrical coordinates)
#     Sigma0: amplitude of the exponential
#     Rd: disk scale length

#     returns: d (Sigma0 * exp(- R / Rd)) / dR
#     """

#     return - Sigma0 * np.exp(- R / Rd ) / Rd

# test_R = np.linspace(1., 30., 10) # kpc
# test_Rd = 4.9 # kpc
# test_q0 = 0.2
# test_Sigma0 = 1e6 * 50.
# test_F = Fr(test_R, sech_drho, (1., exponential_dSigma, (test_Sigma0, test_Rd))) #test_q0 * test_Rd


# print test_F

# plt.scatter(test_R, np.sqrt(- test_R * test_F) * 1e-3)
# plt.yscale('log')
# plt.savefig('deleteme_test_Fr.png')
# plt.clf()

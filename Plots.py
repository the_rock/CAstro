import matplotlib
with open('/home/cstone/Programming/goodies/python_goodies/matplotlib_style.txt', 'r') as f:
    style = eval(f.read())
matplotlib.rcParams.update(style)
import matplotlib.pyplot as plt
import numpy as np

def SB_plot(R, SB, saveto = 'SB_plot.pdf', Re = None, COG = None, R23p5 = None, SB_extra_bands = None, SB_extra_band_labels = None, R_units = 'arcsec', RC = None):
    """
    Surface Brightness Plot

    R: Radius of each sample, in units specified with R_units
    SB: surface brightness samples
    saveto: path to save file
    Re: effective radius (the top x-axis will be shown in units of Re) in the same units as R
    COG: curve of growth samples at the same Radii as SB
    R23p5: isophotal 23.5 mag arcsec^-2 radius in same units as R
    SB_extra_bands: a list containing an arbitrary number of extra SB bands
    SB_extra_band_labels: labels to identify what each SB band is, the first one identifies the band for SB
    R_units: radius units used for R
    RC: rotation curve, input as a dict with 'R' for radius (same units as R) and 'V' for velocity measurements. The RC is preferably folded

    returns: the fig and ax variables if no saveto file is specified, otherwise it saves the plot and returns nothing 
    """
    
    fig, ax = plt.subplots()

    SBlabel = 'SB' if SB_extra_band_labels is None else 'SB %s' % SB_extra_band_labels[0]
    ax.scatter(R, SB, label = None if COG is None and RC is None else SBlabel, s = 30, c = 'C0')
    if not SB_extra_bands is None:
        for i in range(len(SB_extra_bands)):
            ax.scatter(R, SB_extra_bands[i], label = 'SB %s' % SB_extra_band_labels[i+1])
    ax.axvline(R23p5, label = 'R$_{23.5}$', c = 'C0')
    ax.invert_yaxis()
    ax.tick_params(axis = 'y', colors = 'C0')
    ax.set_xlim([0, max(np.abs(R))*1.05])
    ax.set_xlabel('R [%s]' % R_units)
    ax.set_ylabel('$\\mu$ [mag arcsec$^{-2}$]')
    # plt.legend()

    if not COG is None:
        axcog = ax.twinx()
        axcog.scatter(R, COG, c = 'orange', label = 'COG', s = 30)
        axcog.invert_yaxis()
        axcog.tick_params(axis = 'y', colors = 'orange')
        axcog.set_ylabel('m [mag]')

    if not Re is None:
        axre = ax.twiny()
        axre.set_xlim([ax.get_xlim()[0] / Re, ax.get_xlim()[1] / Re])
        axre.set_xlabel('R/R$_{\\rm e}$')

    if not RC is None:
        axrc = ax.twinx()
        axrc.scatter(RC['R'], RC['V'], c = 'r', label = 'RC', s = 10)
        if not COG is None:
            axrc.spines['right'].set_position(('axes',1.2))
            axrc.set_frame_on(True)
            axrc.patch.set_visible(False)
            for sp in axrc.spines.values():
                sp.set_visible(False)
            axrc.spines['right'].set_visible(True)
        axrc.tick_params(axis='y', colors = 'r')
        axrc.set_ylabel('V [km s$^{-1}$]')
        axrc.set_ylim([0,axrc.get_ylim()[1]])

    if saveto is None:
        return fig, ax, None if COG is None else axcog, None if RC is None else axrc, None if Re is None else axre
    else:
        plt.savefig(saveto)
        plt.clf()


import numpy as np
from scipy.special import gamma
from scipy.optimize import minimize
from scipy.stats import iqr
from multiprocessing import Pool
from scipy import odr

def OrthogonalDistanceRegression(X, Y, XE = None, YE = None, function = 'linear', beta0 = [1.,1.], Sigma_clip = None):
    """
    Basic wrapper to apply scipy odr fitting for 2d data with a linear model

    X: x-axis data points [numpy array]
    Y: y-axis data points [numpy array]
    XE: uncertainty in the x-axis points [numpy array]
    YE: uncertainty in the y-axis points [numpy array]
    function: model function for ODR fit [string or python function]
    beta0: initial guess for fit parameters [numpy array]
    Sigma_clip: if set to a number, will clip data after
                fit and then refit [None or number]

    returns: fit parameters array (offset, slope)
    """

    if not XE is None and not YE is None:
        data = odr.RealData(X, Y, sx = XE, sy = YE)
    else:
        data = odr.Data(X,Y)
    if function == 'linear':
        fnc = odr.polynomial(1)
    else:
        fnc = odr.Model(function)
    myodr = odr.ODR(data, fnc, beta0 = beta0)
    res = myodr.run()

    if not Sigma_clip is None:
        if function == 'linear':
            residual = Y - (res.beta[0] + res.beta[1]*X)
        else:
            residual = Y - function(res.beta, X)
        CHOOSE = np.abs(residual) < (Sigma_clip*iqr(residual,rng = [16,84])/2.)
        return OrthogonalDistanceRegression(X[CHOOSE], Y[CHOOSE],
                                            None if XE is None else XE[CHOOSE],
                                            None if YE is None else YE[CHOOSE],
                                            function = function,
                                            beta0 = res.beta,
                                            Sigma_clip = None)
    
    return res.beta 


def sersic_I(R, Re, Ie, n, verbose = False):
    """
    A serseic profile in flux/intensity space. Used to fit elliptical galaxy SB profiles.
    R: radius values to evaluate the sersic function
    Re: width parameter, effective radius (fitted)
    Ie: effective radius flux/intensity (fitted)
    n: sersic index (fitted)
    verbose: warn for sersic index < 1
    """

    if verbose and n < 1:
        print('WARNING: sersic index n < 1')
    b = 1.999 * n - 0.327 # http://people.virginia.edu/~dmw8f/astr5630/Topic07/Lecture_7.html
        
    # https://en.wikipedia.org/wiki/Sersic_profile
    return Ie * np.exp( - b * ((R/Re)**(1.0 / n) - 1.0) )

def sersic_log(R, Re, Ie, n, verbose = False):
    """
    A serseic profile in SB space. Used to fit elliptical galaxy SB profiles.
    Note that SB space is just: mu \propto -2.5 * log10(I)
    R: radius values to evaluate the sersic function
    Re: width parameter, effective radius (fitted)
    Ie: effective radius flux/intensity (fitted)
    n: sersic index (fitted)
    verbose: warn for sersic index < 1
    """

    if verbose and n < 1:
        print('WARNING: sersic index n < 1')
    b = 1.999 * n - 0.327 # http://people.virginia.edu/~dmw8f/astr5630/Topic07/Lecture_7.html

    # https://en.wikipedia.org/wiki/Sersic_profile
    return - 2.5 * (np.log10(Ie) - np.log10(np.e) * b * ((R/Re)**(1.0 / n) - 1.0))

# # Courteau 97 multiparameter model for rotation curves
# ######################################################################
# def multi_model_RC(x, R, zero_point = False, fixed_origin = False):
#     """
#     See Courteau 1997 Model 2 The Multi-Parameter Function
#     x: 0 - r_t, the transition radius from rising to flat
#        1 - x0, the x-axis offset for the center of the galaxy
#        2 - v0, the y-axis offset for the zero of the rotation curve
#        3 - v_c, asymptotic velocity unless beta is used
#        4 - beta, used to model a dropping rotation curve
#        5 - gamma, governs the sharpness of the turnover at r_t
#     """
#     sign = np.ones(len(R))
#     if fixed_origin:
#         y = np.abs(x[0] / R)
#         sign[R < 0] = -1
#         return sign * x[3] * ((1. + y)**x[4]) / ((1. + y**x[5])**(1./x[5]))
#     else:
#         y = np.abs(x[0] / (R)) # -x[1]
#         sign[R < 0] = -1 # x[1]
#         return x[2] + sign * x[3] * ((1. + y)**x[4]) / ((1. + y**x[5])**(1./x[5]))

# def multi_loss_median_RC(x, R, V, E, C, fixed_origin):
#     x[1] = 0
#     if fixed_origin:
#         x[2] = 0
#     if E is None:
#         E = np.ones(len(R))
#     else:
#         E = np.clip(E, a_min = 3., a_max = None)
#     sign = np.sign(np.sum(np.array(R)*(np.array(V)-np.mean(V))))
#     V_model = multi_model_RC(x, R, fixed_origin = fixed_origin)
#     CHOOSE = np.isfinite(V_model)
#     if np.sum(CHOOSE) <= 0:
#         return 1e9
#     losses = ((V - V_model)/E)[CHOOSE]
    
#     return np.median(losses)**2 + iqr(losses, rng = [20, 80])**2
    
# def multi_loss_RC(x, R, V, E, C, fixed_origin):
#     x[1] = 0
#     if fixed_origin:
#         x[2] = 0
#     if E is None:
#         E = np.ones(len(R))
#     else:
#         E = np.clip(E, a_min = 3., a_max = None)
#     sign = np.sign(np.sum(np.array(R)*(np.array(V)-np.mean(V))))
#     V_model = multi_model_RC(x, R, fixed_origin = fixed_origin)
#     CHOOSE = np.isfinite(V_model)
#     if np.sum(CHOOSE) <= 0:
#         return 1e9
#     N = np.argsort(abs((V - V_model)/E)[CHOOSE])
#     Nreg = np.argsort(V)
#     Nabs = np.argsort(np.abs(V))
#     model_loss = np.mean(((V - V_model)/E)[CHOOSE][N[:-2]]**2)
#     regularization = C*np.sum(np.array([np.log(abs(2.*x[0]/R[Nabs[-2]])),
#                                         (x[1] - (0. if fixed_origin else 10.0))/10.,
#                                         (x[2] - (0. if fixed_origin else (V[Nreg[-2]] + V[Nreg[1]])/2.))/10.,
#                                         np.log(abs(x[3]/(V[Nreg[-2]] if fixed_origin else (V[Nreg[-2]] - V[Nreg[1]])/2.))),
#                                         x[4],
#                                         x[5]/20.])**2)
#     return model_loss + regularization #C*np.sum((np.array(x) / np.array([R[np.argmax(np.abs(V))], 1e-9 if fixed_origin else 10.0, 1e-9 if fixed_origin else np.mean(V), np.max(V) if fixed_origin else (np.max(V)-V[N[1]])/2., 0.1, 10.0]))**2)

# def multi_fit_RC(R,V, E = None, x0 = None, C = 1.0, fixed_origin = False):
    
#     sign = np.sign(np.sum(np.array(R)*(np.array(V) - np.mean(V))))#-1. if V[0] > V[-1] else 1.
#     if x0 is None:
#         N = np.argsort(V)
#         Nabs = np.argsort(np.abs(V))
#         x0_1 = [np.abs(R[Nabs[-2]]), 0.0, (V[N[-2]] + V[N[1]])/2., sign*(V[N[-2]] - V[N[1]])/2., 0.0, 20.]
#         x0_2 = [np.abs(R[Nabs[-2]])/1.5, 0.0, np.mean(V), 1.1*sign*(V[N[-2]] - V[N[1]])/2., 0.0, 10.]
#         x0_3 = [np.abs(R[Nabs[-2]])*1.5, 0.0, 1.1*(V[N[-2]] + V[N[1]])/2., sign*(V[N[-2]] - V[N[1]])/(2.*1.1), 0.1, 1.]
#         res_1 = multi_fit_RC(R,V, E = E, x0 = x0_1, C = C, fixed_origin = fixed_origin)
#         res_2 = multi_fit_RC(R,V, E = E, x0 = x0_2, C = C, fixed_origin = fixed_origin)
#         res_3 = multi_fit_RC(R,V, E = E, x0 = x0_3, C = C, fixed_origin = fixed_origin)
#         res_4 = multi_fit_RC(R, V, E = E, x0 = x0_1, C = 0., fixed_origin = fixed_origin)
#         loss_1 = multi_loss_RC(res_1, R, V, E, C, fixed_origin)
#         loss_2 = multi_loss_RC(res_2, R, V, E, C, fixed_origin)
#         loss_3 = multi_loss_RC(res_3, R, V, E, C, fixed_origin)
#         loss_4 = multi_loss_RC(res_4, R, V, E, C, fixed_origin)
#         res = []
#         loss = []
#         if np.isfinite(loss_1):
#             loss.append(loss_1)
#             res.append(res_1)
#         if np.isfinite(loss_2):
#             loss.append(loss_2)
#             res.append(res_2)
#         if np.isfinite(loss_3):
#             loss.append(loss_3)
#             res.append(res_3)
#         if np.isfinite(loss_4):
#             loss.append(loss_4)
#             res.append(res_4)
#         if len(loss) == 0:
#             print(str(R) + str(V) + str(fixed_origin) + str([x0_1, x0_2, x0_3]))
#         return res[np.argmin(loss)]
#     x0[1] = 0
#     if fixed_origin:
#         x0[2] = 0
#         x0[3] = sign*np.abs(V[np.argsort(V)[-2]])
#         x0[5] = 10
#     res = minimize(fun = multi_loss_RC, x0 = x0, args = (np.array(R),np.array(V), E, C, fixed_origin), method = 'Nelder-Mead') # , bounds = [(0.001, None), (None, None), (None, None), (-1000, 1000), (-10, 10), (0.001,100)]
#     res = minimize(fun = multi_loss_median_RC, x0 = res.x, args = (np.array(R),np.array(V), E, C, fixed_origin), method = 'Nelder-Mead')

#     x = res.x
#     x[1] = 0
#     if fixed_origin:
#         x[2] = 0
#     return x

# # Courteau 97 Model (different style)
# ######################################################################
# def courteau97_model(x, R):
#     """
#     See Courteau 1997 Model 2 The Multi-Parameter Function
#     x: 0 - r_t, the transition radius from rising to flat
#        1 - beta, used to model a dropping rotation curve
#        2 - gamma, governs the sharpness of the turnover at r_t
#        3 - v_c, asymptotic velocity unless beta is used
#        4 - x0, the x-axis offset for the center of the galaxy
#        5 - v0, the y-axis offset for the zero of the rotation curve
#     R: radius values

#     returns: Velocities at points R, given model parameters x
#     """
#     sign = np.ones(len(R - x[4]))
#     y = np.abs(x[0] / (R - x[4]))
#     sign[(R-x[4]) < 0] = -1
#     return x[5] + sign * x[3] * ((1. + y)**x[1]) / ((1. + y**x[2])**(1./x[2]))

# def courteau97_chi2(x, R, V, E, fixed_v0, fixed_x0):
#     ndf = len(R) - (6 - (1. if fixed_v0 else 0.) - (1. if fixed_x0 else 0.))
#     chi2 = np.sum((V - courteau97_model(x, R))**2 / np.clip(E, a_min = 3., a_max = None)**2)
#     return chi2 / ndf

# def courteau97_loss(x, R, V, E, fixed_v0, fixed_x0, C, x0):
#     comparisons = (V - courteau97_model(x, R))**2 / np.clip(E, a_min = 3., a_max = None)**2
#     ndf = len(R) - (6 - (1. if fixed_v0 else 0.) - (1. if fixed_x0 else 0.))
#     robust_reducedchi2 = (np.sum(comparisons[np.argsort(comparisons)[:int(len(R)*0.9)]])/0.9) / ndf
#     regularization = 0. if C == 0. else sum(np.array([np.log10(x[0]/x0[0]), x[1], np.log(x[2]/x0[2]), np.log(x[3]/x0[3]), (x[4] - x0[4])*10/(np.max(R) - np.min(R)), (x[5] - x0[5]) * 10 /(np.max(V) - np.min(V))])**2)
#     return robust_reducedchi2 + C * regularization

# def convert_x0(x, fixed_v0, fixed_x0):
#     if fixed_v0 and fixed_x0:
#         return list(x) + [0., 0.]
#     elif fixed_x0:
#         return list(x[:-1]) + [0., x[-1]]
#     elif fixed_v0:
#         return list(x) + [0.]
#     else:
#         return x

# def wrap_courteau97_loss(a,b,c,d,e,f,g,h):
#     return courteau97_loss(convert_x0(a, e,f), b,c,d,e,f,g,h)
# def wrap_minimize_pool(x):
#     return minimize(fun = x[0], x0 = x[1], args = x[2])
    
# def courteau97_fit(R, V, E, x0 = None, fixed_v0 = False, fixed_x0 = False, optimize_C = False):

#     sign = np.sign(np.sum(np.array(R)*(np.array(V) - np.median(V))))
#     if x0 is None:
#         v0 = 0. if fixed_v0 else (V[np.argsort(V)[-2]] + V[np.argsort(V)[1]])/2.
#         x0 = [np.abs(R[np.argsort(np.abs(V - v0))[-2]])/2.,
#               0.,
#               10.,
#               sign*(V[np.argsort(V)[-2]] if fixed_v0 else (V[np.argsort(V)[-2]] - V[np.argsort(V)[1]])/2.)] +\
#               ([] if fixed_x0 else [R[np.argmin(np.abs(V - v0))]]) +\
#               ([] if fixed_v0 else [v0])
#     x0 = np.array(x0)
#     p = Pool(5)
#     arg = list([wrap_courteau97_loss, x0 * np.random.uniform(0.5, 2, len(x0)) + np.random.normal(0, 1., len(x0)), (np.array(R),np.array(V), np.array(E) if type(E) == list else E, fixed_v0, fixed_x0, 10., convert_x0(x0, fixed_v0, fixed_x0))] for i in range(5))
#     res = p.map(wrap_minimize_pool, arg)
#     chi2 = []
#     for i in range(len(res)):
#         x = convert_x0(res[i].x, fixed_v0, fixed_x0)
#         chi2.append(courteau97_chi2(x, R, V, E, fixed_v0, fixed_x0))
#     res = res[np.argmin(chi2)]
#     #res = minimize(fun = wrap_courteau97_loss, x0 = x0, args = (np.array(R),np.array(V), np.array(E) if type(E) == list else E, fixed_v0, fixed_x0, 10., convert_x0(x0, fixed_v0, fixed_x0)))
#     x1 = convert_x0(res.x, fixed_v0, fixed_x0)
        
#     return x1, courteau97_chi2(x1, R, V, E, fixed_v0, fixed_x0)
    

# # tangent model for RCs
# ######################################################################

# def tan_model_RC(x, R):
#     """
#     Model that generates velocity values given parameters x and radius R.

#     x: parameters for model:
#          0 - radial scaling (Rd)
#          1 - vertical scaling (Vflat)
#          2 - horizontal shift (misaligned slit)
#          3 - vertical shift (hubble flow)
#     R: radius to evaluate the model (same units as x)

#     returns: Velocity (same units as x)
#     """

#     return x[1] * np.arctan(R / x[0]) + x[3] # + x[2]

# def tan_loss_RC(x, R, V, fixed_origin = False):

#     return np.mean((V - tan_model_RC([x[0],x[1],0.,0.] if fixed_origin else x, R))**2)

# def tan_fit_RC(R,V, x0 = None, fixed_origin = False):

#     if x0 is None:
#         sign = -1. if V[0] > V[-1] else 1.
#         x0 = [np.mean(np.abs(R)), (max(V) - min(V))/2., 0., np.mean(V)]

#     res = minimize(fun = tan_loss_RC, x0 = x0, args = (R,V,fixed_origin), method = 'Nelder-Mead')

#     x = res.x
#     x[2] = 0.
#     if fixed_origin:
#         x[3] = 0.
#     return x

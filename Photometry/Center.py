import numpy as np
from photutils.centroids import centroid_2dg, centroid_com, centroid_1dg
from astropy.visualization import SqrtStretch, LogStretch
from astropy.visualization.mpl_normalize import ImageNormalize
import matplotlib.pyplot as plt
import logging

def Center_Null(IMG, seeing, pixscale, background, psf, mask, name, **kwargs):
    """
    Simply returns the center of the image

    IMG: numpy 2d array of pixel values
    seeing: the seeing conditions that the image was taken in (arcsec)
    pixelscale: conversion factor from pixels to arcsec (arcsec pixel^-1)
    background: output from a image background signal calculation (dict)
    mask: output from mask calculations (dict)    
    """

    return {'x': IMG.shape[0]/2.,
            'y': IMG.shape[1]/2.}

def GivenCenter(IMG, seeing, pixscale, background, psf, mask, name, **kwargs):
    """
    Uses the kwarg "given_centers" to return a user inputted center.
    The given_centers object should be a dictionary where each key
    is a galaxy name (As in the names given for each galaxy) and the
    value is a dictionary structured as {'x': float, 'y': float}
    where the floats are the center coordinates in pixels. If the
    galaxy name is not in the given_centers dictionary it will return
    the center of the image.

    IMG: numpy 2d array of pixel values
    seeing: the seeing conditions that the image was taken in (arcsec)
    pixelscale: conversion factor from pixels to arcsec (arcsec pixel^-1)
    background: output from a image background signal calculation (dict)
    mask: output from mask calculations (dict)        
    """

    try:
        return kwargs['given_centers'][name]
    except:
        return {'x': IMG.shape[0]/2.,
                'y': IMG.shape[1]/2.}
    
    
def Center_Centroid(IMG, seeing, pixscale, background, psf, mask, name, **kwargs):
    """
    Compute the pixel location of the galaxy center using a centroid method.
    Looking at 50 seeing lengths around the center of the image (images
    should already be mostly centered), finds the galaxy center by fitting
    a 2d Gaussian.
    
    IMG: numpy 2d array of pixel values
    seeing: the seeing conditions that the image was taken in (arcsec)
    pixelscale: conversion factor from pixels to arcsec (arcsec pixel^-1)
    background: output from a image background signal calculation (dict)
    mask: output from mask calculations (dict)
    """
    
    # Create mask to focus centering algorithm on the center of the image
    centralize_mask = np.ones(IMG.shape)
    centralize_mask[int(IMG.shape[0]/2 - 30 * seeing / pixscale):int(IMG.shape[0]/2 + 30 * seeing / pixscale),
                    int(IMG.shape[1]/2 - 30 * seeing / pixscale):int(IMG.shape[1]/2 + 30 * seeing / pixscale)] = 0
    decentralize_mask = np.ones(IMG.shape)
    decentralize_mask[int(IMG.shape[0]/2 - 5 * seeing / pixscale):int(IMG.shape[0]/2 + 5 * seeing / pixscale),
                      int(IMG.shape[1]/2 - 5 * seeing / pixscale):int(IMG.shape[1]/2 + 5 * seeing / pixscale)] = 0
    
    x, y = centroid_2dg(IMG - background['median'],
                        mask = np.logical_and(np.logical_or(np.logical_or(mask['mask'],
                                                                          mask['overflow mask']),
                                                            centralize_mask),
                                              decentralize_mask))

    # Plot center value for diagnostic purposes
    if 'doplot' in kwargs and kwargs['doplot']:    
        plt.imshow(np.clip(IMG - background['median'],a_min = 0, a_max = None),
                   origin = 'lower', cmap = 'Greys_r', norm = ImageNormalize(stretch=LogStretch()))
        plt.plot([y],[x], marker = 'x', markersize = 10, color = 'y')
        plt.savefig('%scenter_vis_%s.png' % (kwargs['plotpath'] if 'plotpath' in kwargs else '', name))
        plt.clf()
    logging.info('%s Center found: x %.1f, y %.1f' % (name, x, y))    
    return {'x': x,
            'y': y}

def Center_1DGaussian(IMG, seeing, pixscale, background, psf, mask, name, **kwargs):
    """
    Compute the pixel location of the galaxy center using a photutils method.
    Looking at 100 seeing lengths around the center of the image (images
    should already be mostly centered), finds the galaxy center by fitting
    several 1d Gaussians.
    
    IMG: numpy 2d array of pixel values
    seeing: the seeing conditions that the image was taken in (arcsec)
    pixelscale: conversion factor from pixels to arcsec (arcsec pixel^-1)
    background: output from a image background signal calculation (dict)
    mask: output from mask calculations (dict)
    """
    
    # mask image to focus algorithm on the center of the image
    centralize_mask = np.ones(IMG.shape, dtype = bool)
    centralize_mask[int(IMG.shape[0]/2 - 100 * seeing / pixscale):int(IMG.shape[0]/2 + 100 * seeing / pixscale),
                    int(IMG.shape[1]/2 - 100 * seeing / pixscale):int(IMG.shape[1]/2 + 100 * seeing / pixscale)] = False
    
    x, y = centroid_1dg(IMG - background['median'],
                        mask = centralize_mask) # np.logical_or(mask['mask'], centralize_mask)
    
    # Plot center value for diagnostic purposes
    if 'doplot' in kwargs and kwargs['doplot']:    
        plt.imshow(np.clip(IMG - background['median'],a_min = 0, a_max = None),
                   origin = 'lower', cmap = 'Greys_r', norm = ImageNormalize(stretch=LogStretch()))
        plt.plot([y],[x], marker = 'x', markersize = 10, color = 'y')
        plt.savefig('%scenter_vis_%s.png' % (kwargs['plotpath'] if 'plotpath' in kwargs else '', name))
        plt.clf()
    logging.info('%s Center found: x %.1f, y %.1f' % (name, x, y))    
    return {'x': x,
            'y': y}

def Center_OfMass(IMG, seeing, pixscale, background, psf, mask, name, **kwargs):
    """
    Compute the pixel location of the galaxy center using a light weighted
    center of mass. Looking at 100 seeing lengths around the center of the
    image (images should already be mostly centered), finds the average
    light weighted center of the image.
    
    IMG: numpy 2d array of pixel values
    seeing: the seeing conditions that the image was taken in (arcsec)
    pixelscale: conversion factor from pixels to arcsec (arcsec pixel^-1)
    background: output from a image background signal calculation (dict)
    mask: output from mask calculations (dict)
    """
    
    
    # mask image to focus algorithm on the center of the image
    centralize_mask = np.ones(IMG.shape)
    centralize_mask[int(IMG.shape[0]/2 - 50 * seeing / pixscale):int(IMG.shape[0]/2 + 50 * seeing / pixscale),
                    int(IMG.shape[1]/2 - 50 * seeing / pixscale):int(IMG.shape[1]/2 + 50 * seeing / pixscale)] = 0
    
    x, y = centroid_com(IMG - background['median'],
                        mask = centralize_mask) # np.logical_or(mask['mask'], centralize_mask)
    
    # Plot center value for diagnostic purposes
    if 'doplot' in kwargs and kwargs['doplot']:    
        plt.imshow(np.clip(IMG - background['median'],a_min = 0, a_max = None),
                   origin = 'lower', cmap = 'Greys_r', norm = ImageNormalize(stretch=LogStretch()))
        plt.plot([y],[x], marker = 'x', markersize = 10, color = 'y')
        plt.savefig('%scenter_vis_%s.png' % (kwargs['plotpath'] if 'plotpath' in kwargs else '', name))
        plt.clf()
    logging.info('%s Center found: x %.1f, y %.1f' % (name, x, y))    
    return {'x': x,
            'y': y}

def Center_Multi_Method(IMG, seeing, pixscale, background, psf, mask, name, **kwargs):
    """
    Compute the pixel location of the galaxy center using the other methods
    included here, determines the best one to use.
    
    IMG: numpy 2d array of pixel values
    seeing: the seeing conditions that the image was taken in (arcsec)
    pixelscale: conversion factor from pixels to arcsec (arcsec pixel^-1)
    background: output from a image background signal calculation (dict)
    mask: output from mask calculations (dict)
    """

    # Centering algorithm order for multi-method
    cents = ['Centroid', '1D', 'COM']
    cent_fun = [Center_Centroid, Center_1DGaussian, Center_OfMass]
    x_colours = ['y', 'cyan', 'magenta', 'lime']

    # Meshgrid from relative pixel locations
    XX, YY = np.meshgrid(range(IMG.shape[0]), range(IMG.shape[1]), indexing = 'xy')
    CenterR = np.sqrt((XX - IMG.shape[0]/2)**2 + (YY - IMG.shape[1]/2)**2)
    
    cent_vals = []
    for i in range(len(cents)):
        # Run the centering algorithm
        try:
            cent = cent_fun[i](IMG, seeing, pixscale, background, psf, mask, name, **kwargs)
        except:
            cent = {'x': np.nan, 'y': np.nan}
        cent_vals.append(cent)

        # Check that the centering algorithm didn't crash
        if not (np.isfinite(cent['x']) and np.isfinite(cent['y']) and 0 <= cent['x'] < IMG.shape[0] and 0 <= cent['y'] < IMG.shape[1]):
            continue
        # Evaluate relative pixel locations to center
        R = np.sqrt((XX - cent['x'])**2 + (YY - cent['y'])**2)
        # Check how the algorithm center brightness compares to the image center brightness
        if np.sqrt((cent['x'] - IMG.shape[0]/2)**2 + (cent['y'] - IMG.shape[1]/2)**2) < 10*seeing/pixscale:
            # Plot center for diagnostic purposes
            if 'doplot' in kwargs and kwargs['doplot']:    
                plt.imshow(np.clip(IMG,a_min = 0, a_max = None), origin = 'lower',
                           cmap = 'Greys_r', norm = ImageNormalize(stretch=LogStretch()))
                for vi,v in enumerate(cent_vals):
                    plt.plot([v['x']],[v['y']], marker = 'x', markersize = 10, color = x_colours[vi], label = cents[vi])
                plt.legend()
                plt.savefig('%scenter_vis_%s.png' % (kwargs['plotpath'] if 'plotpath' in kwargs else '', name))
                plt.clf()
            return cent
    
    logging.warning('%s Centering failed, using center of image' % name)
    return {'x':int(IMG.shape[0]/2.),
            'y': int(IMG.shape[1]/2.)}

import numpy as np
from photutils.isophote import EllipseSample, Ellipse, EllipseGeometry, Isophote, IsophoteList
from scipy.optimize import minimize
from scipy.stats import iqr
from time import time
from astropy.visualization import SqrtStretch, LogStretch
from astropy.visualization.mpl_normalize import ImageNormalize
import matplotlib.pyplot as plt
from matplotlib.patches import Ellipse
from copy import copy
import logging
from .Elliptical_Isophotes import _x_to_pa, _x_to_eps

def Simple_Isophote_Extract(IMG, mask, background_level, center, R, E, PA, name = ''):
    """
    Extracts the specified isophotes using photutils ellipsesample function.
    Applies mask and backgorund level to image.    
    """

    # Create image array with background and mask applied
    if np.any(mask):
        logging.info('%s: is masked' % (name))
        dat = np.ma.masked_array(IMG - background_level, mask)
    else:
        logging.info('%s: is not masked' % (name))
        dat = IMG - background_level
        
    # Store isophotes
    iso_list = []
    
    for i in range(len(R)):
        # Container for ellipse geometry
        geo = EllipseGeometry(sma = R[i],
                              x0 = center['x'], y0 = center['y'],
                              eps = E[i], pa = PA[i])
        # Extract the isophote information
        ES = EllipseSample(dat, sma = R[i], geometry = geo)
        ES.extract()
        iso_list.append(Isophote(ES, niter = 30, valid = True, stop_code = 0))
        
    return IsophoteList(iso_list)

def Isophote_Extract(IMG, seeing, pixscale, background, psf, mask, center, isofit, name, **kwargs):
    """
    Extract isophotes given output profile from Isophotes_Simultaneous_Fit which
    parametrizes pa and ellipticity via functions which map all the reals to the
    appropriate parameter range. This function also extrapolates the profile to
    large and small radii (simply by taking the parameters at the edge, no
    functional extrapolation). By default uses a linear radius growth, however
    for large images, it uses a geometric radius growth of 10% per isophote.
    """

    # Radius values to evaluate isophotes
    if 'samplestyle' in kwargs and kwargs['samplestyle'] == 'geometric-linear':
        R = [kwargs['sampleinitR'] if 'sampleinitR' in kwargs else 0.5*seeing/pixscale]
        R.append(R[-1]*(1. + (kwargs['samplegeometricscale'] if 'samplegeometricscale' in kwargs else 0.1)))
        while R[-1] < (kwargs['sampleendR'] if 'sampleendR' in kwargs else IMG.shape[0]/2):
            if (abs(R[-1] - R[-2])+1e-4) >= (kwargs['samplelinearscale'] if 'samplelinearscale' in kwargs else seeing/pixscale):
                R.append(R[-1] + (kwargs['samplelinearscale'] if 'samplelinearscale' in kwargs else seeing/pixscale))
            else:
                R.append(R[-1]*(1. + (kwargs['samplegeometricscale'] if 'samplegeometricscale' in kwargs else 0.1)))
        R = np.array(R)
    elif 'samplestyle' in kwargs and kwargs['samplestyle'] == 'linear':
        R = np.arange(kwargs['sampleinitR'] if 'sampleinitR' in kwargs else 0.5*seeing/pixscale,
                      kwargs['sampleendR'] if 'sampleendR' in kwargs else IMG.shape[0]/2,
                      kwargs['samplelinearscale'] if 'samplelinearscale' in kwargs else seeing/pixscale)
    else:
        R = [kwargs['sampleinitR'] if 'sampleinitR' in kwargs else 0.5*seeing/pixscale]
        while R[-1] < (kwargs['sampleendR'] if 'sampleendR' in kwargs else IMG.shape[0]/2):
            R.append(R[-1]*(1. + (kwargs['samplegeometricscale'] if 'samplegeometricscale' in kwargs else 0.1)))
        R = np.array(R)
    logging.info('%s: R complete in range [%.1f,%.1f]' % (name,R[0],R[-1]))
    
    # Interpolate profile values, when extrapolating just take last point
    E = _x_to_eps(np.interp(R, isofit['ellipse_radii'], isofit['X'][0]))
    E[R < isofit['ellipse_radii'][0]] = R[R < isofit['ellipse_radii'][0]] * _x_to_eps(isofit['X'][0][0]) / isofit['ellipse_radii'][0]
    E[R > isofit['ellipse_radii'][-1]] = _x_to_eps(isofit['X'][0][-1])
    PA = _x_to_pa(np.interp(R, isofit['ellipse_radii'], isofit['X'][1]))
    PA[R < isofit['ellipse_radii'][0]] = _x_to_pa(isofit['X'][1][0])
    PA[R > isofit['ellipse_radii'][-1]] = _x_to_pa(isofit['X'][1][-1])

    # Stop from masking anything at the center
    mask['mask'][int(center['x'] - 10*psf['median']):int(center['x'] + 10*psf['median']),
                 int(center['y'] - 10*psf['median']):int(center['y'] + 10*psf['median'])] = False
    compund_Mask = np.logical_or(mask['overflow mask'],mask['mask'])
    
    return Simple_Isophote_Extract(IMG,
                                   compund_Mask,
                                   background['median'],
                                   center, R, E, PA, name)
    

import sys
import os
sys.path.append(os.environ['CAstro'])
from Photometry.Background import Background_Global, Background_ByPatches, Background_ByIsophote
from Photometry.Center import Center_Centroid, Center_OfMass, Center_Multi_Method, Center_1DGaussian, Center_Null
from Photometry.PSF import Calculate_PSF
from Photometry.Mask import Star_Mask_IRAF, NoMask
from Photometry.Elliptical_Isophotes import Fit_Isophotes, Isophotes_Simultaneous_Fit, Photutils_Fit
from Photometry.Isophote_Extract import Isophote_Extract
from Photometry_Profile_Conversion import SBprof_to_COG_errorprop
from multiprocessing import Pool
from astropy.io import fits
from scipy.stats import iqr
from itertools import starmap
import numpy as np
from time import time, sleep
import traceback
import logging

class Isophote_Pipeline(object):

    def __init__(self, set_background_f = None, set_psf_f = None, set_starmask_f = None, set_center_f = None,
                 set_isophotefit_f = None, set_profextract_f = None, loggername = None):
        """
        Initialize pipeline object, user can replace functions with their own if they want, otherwise defaults are used.

        set_background_f: Set the function for computing the background/sky level of an image
        set_center_f: Set the function for determining the center of the galaxy
        set_psf_f: Set the function for computing the Point Spread Function (PSF) of the image
        set_starmask_f: Set function for masking stars from an image, default no mask since SBs are robust median
        set_isophotefit_f: Set function for fitting isophotes to the galaxy
        set_profextract_f: Set function that extracts the profile from the image given isophote parameters
        loggername: String to use for logging messages
        """
        
        # Set the functions for this pipeline, use defaults if none provided
        self.background_f = Background_Global if set_background_f is None else set_background_f
        self.psf_f = Calculate_PSF if set_psf_f is None else set_psf_f
        self.starmask_f = NoMask if set_starmask_f is None else set_starmask_f
        self.center_f = Center_Multi_Method if set_center_f is None else set_center_f
        self.isophotefit_f = Fit_Isophotes if set_isophotefit_f is None else set_isophotefit_f
        self.profextract_f = Isophote_Extract if set_profextract_f is None else set_profextract_f

        # Start the logger
        logging.basicConfig(level=logging.INFO, filename = 'Log_IsoPIPELINE.log' if loggername is None else loggername, filemode = 'w')
        

    def Read_Image(self, filename, **kwargs):
        """
        Reads a galaxy image given a file name. In a fits image the data is assumed to exist in the
        primary HDU unless given 'hdulelement'. In a numpy file, it is assumed that only one image
        is in the file.

        filename: A string containing the full path to an image file

        returns: Extracted image data as numpy 2D array
        """

        # Read a fits file
        if filename[filename.rfind('.')+1:].lower() == 'fits':
            hdul = fits.open(filename)
            dat = hdul[kwargs['hdulelement'] if 'hdulelement' in kwargs else 0].data
        # Read a numpy array file
        if filename[filename.rfind('.')+1:].lower() == 'npy':
            dat = np.load(filename)
            
        return dat
        
    def WriteProf(self, isolist, saveto, pixscale, starmask = None, background = None, name = None, **kwargs):
        """
        Writes the photometry information for disk given a photutils isolist object

        isolist: photutils iso list object containing isophote objects
        saveto: Full path string indicating where to save the profile
        pixscale: conversion factor between pixels and arcseconds (arcsec / pixel)
        starmask: Optional, a star mask to save along with the profile
        background: if saving the star mask, the background can also be saved
                    for full reproducability
        """

        # Remove file type indicator from saveto path
        if saveto[-4:] == '.txt' or saveto[-5:] == '.prof':
            saveto = saveto[:saveto.rfind('.')]
            
        # Write the profile
        with open(saveto + '.prof', 'w') as f:
            # Write profile header
            f.write('r,sb,sb_e,mag,mag_e,ellip,ellip_e,pa,pa_e,mag_direct,mag_direct_e,x0,y0\n')
            f.write('arcsec,mag arcsec^-2,unitless,mag,unitless,unitless,unitless,deg,deg,mag,unitless,pix,pix\n')
            fillstring = '%f,%f,%f,%f,%f,%.3f,%.3f,%.2f,%.2f,%f,%f,%.1f,%.1f\n'

            # Compute surface brightness in mag arcsec^-2 from flux
            sb = np.array(list((-2.5*np.log10(np.median(isolist.sample[i].values[2]))\
                                + 22.5 + 2.5*np.log10(pixscale**2)) \
                               for i in range(len(isolist.sma))))
            sb[np.logical_not(np.isfinite(sb))] = 99.999
            sbE = list((iqr(isolist.sample[i].values[2],
                            rng = (31.7310507863/2,
                                   100 - 31.7310507863/2)) / (2*np.sqrt(len(isolist.sample[i].values[2])))) \
                       for i in range(len(isolist.sma)))
            sbE = np.array(list((np.abs(2.5*sbE[i]/(np.median(isolist.sample[i].values[2]) * np.log(10)))) \
                                for i in range(len(isolist.sma))))
            sb[np.logical_not(np.isfinite(sbE))] = 99.999
            sbE[np.logical_not(np.isfinite(sbE))] = 99.999

            # Compute Curve of Growth from SB profile
            cog, cogE = SBprof_to_COG_errorprop(isolist.sma * pixscale, sb, sbE, 1. - isolist.eps,
                                                isolist.ellip_err, N = 100, method = 0, symmetric_error = True)
            if cog is None:
                logging.error('%s: Photometry failed, SB profile is nans or all > 50' % str(name))
                return

            # For each radius evaluation, write the profile parameters
            count_fail = 0
            for i in range(len(isolist.sma)):
                tflux_e_err = isolist.rms[i] / (np.sqrt(isolist.npix_e[i]))
                f.write(fillstring % (isolist.sma[i] * pixscale, sb[i], sbE[i],
                                      cog[i], cogE[i], isolist.eps[i], isolist.ellip_err[i],
                                      isolist.pa[i]*180/np.pi, isolist.pa_err[i]*180/np.pi,
                                      22.5 - 2.5*np.log10(isolist.tflux_e[i]),
                                      np.abs(2.5*tflux_e_err/(isolist.tflux_e[i] * np.log(10))),
                                      isolist.x0[i],
                                      isolist.y0[i]))
                if 'sampleerrorlim' in kwargs and sbE[i] > kwargs['sampleerrorlim']:
                    count_fail += 1
                else:
                    count_fail = 0
                if count_fail > 5:
                    break
        # Write the mask data, if provided
        if not starmask is None:
            header = fits.Header()
            header['IMAGE 1'] = 'star mask'
            header['IMAGE 2'] = 'overflow values mask'
            if not background is None:
                for key in background.keys():
                    header['bkgnd %s' % key] = str(background[key])
            hdul = fits.HDUList([fits.PrimaryHDU(header=header),
                                 fits.ImageHDU(starmask['mask'].astype(int)),
                                 fits.ImageHDU(starmask['overflow mask'].astype(int))])
            hdul.writeto(saveto + '_mask.fits', overwrite = True)
            sleep(2)
            # Zip the mask file because it can be large and take a lot of memory, but in principle
            # is very easy to compress
            os.system('gzip -fq '+ saveto + '_mask.fits')

    def Process_Image(self, IMG, seeing, pixscale, saveto = None, name = None, overflowval = None, kwargs_internal = {}, **kwargs):
        """
        Function which runs the pipeline for a single image. Each sub-function of the pipeline is run
        in order and the outputs are passed along. If multiple images are given, the pipeline is
        excecuted on the first image and the isophotes are applied to the others.
        
        IMG: string or list of strings providing the path to an image file
        seeing: image seeing in arcsec, used to set the scale for certain elements of the analysis
        pixscale: angular size of the pixels in arcsec/pixel
        saveto: string or list of strings indicating where to save profiles
        name: string name of galaxy in image, used for log files to make searching easier
        overflowval: pixel flux value for oversaturated pixels

        returns 0 for successful completion of processing
        """

        kwargs.update(kwargs_internal)
        
        # Assign a random integer if no name is given
        if name is None:
            name = '%.6i' % np.random.randint(1e6)

        # Read the primary image
        if type(IMG) == list:
            dat = self.Read_Image(IMG[0], **kwargs)
        elif type(IMG) == str:
            dat = self.Read_Image(IMG, **kwargs)
            IMG = [IMG]
            saveto = [saveto]
        else:
            dat = IMG
            saveto = [saveto]
            
        # Check that image data exists and is not corrupted
        if dat is None or np.all(dat[int(len(dat)/2.)-10:int(len(dat)/2.)+10, int(len(dat[0])/2.)-10:int(len(dat[0])/2.)+10] == 0):
            logging.error('%s Large chunk of data missing, impossible to process image' % name)
            return 1
        # Save profile to the same folder as the image if no path is provided
        if saveto[0] is None:
            saveto[0] = IMG[0][:IMG[0].rfind('.')] + '.prof'
            
        # Track time to run analysis
        start = time()
        
        # Run the Pipeline
        try:
            logging.info('%s: background at: %.1f' % (name, time() - start))
            background_res = self.background_f(dat, seeing, pixscale, name, **kwargs)
            logging.info('%s: psf at: %.1f' % (name, time() - start))
            psf_res = self.psf_f(dat, seeing, pixscale, background_res, name, **kwargs)
            logging.info('%s: star mask at: %.1f' % (name, time() - start))
            starmask_res = self.starmask_f(dat, seeing, pixscale, background_res, psf_res, name, overflowval = overflowval, **kwargs)
            logging.info('%s: center at: %.1f' % (name, time() - start))
            center_res = self.center_f(dat, seeing, pixscale, background_res, psf_res, starmask_res, name, **kwargs)
            logging.info('%s: isophote fit at: %.1f' % (name, time() - start))
            isophotefit_res = self.isophotefit_f(dat, seeing, pixscale, background_res, psf_res, starmask_res, center_res, name, **kwargs)
            logging.info('%s: isophote extract at: %.1f' % (name, time() - start))
            isophoteextract_res = self.profextract_f(dat, seeing, pixscale, background_res, psf_res, starmask_res, center_res, isophotefit_res, name, **kwargs)
        except Exception as e:
            logging.error('%s: %s' % (name, str(e)))
            return 1

        # Save the profile
        logging.info('%s: saving at: %.1f' % (name, time() - start))
        self.WriteProf(isophoteextract_res, saveto[0], pixscale, starmask = starmask_res, background = background_res, name = name, **kwargs)
        
        
        for i in range(len(saveto)):
            if i == 0:
                continue
            # Read secondary images
            dat = self.Read_Image(IMG[i])
            # Process the new image
            try:
                background_res = self.background_f(dat, seeing, pixscale, name, **kwargs)
                isophoteextract_res = self.profextract_f(dat, seeing, pixscale, background_res, psf_res, starmask_res, center_res, isophotefit_res, name, **kwargs)
            except Exception as e:
                logging.error('%s: extra band failed: %s' % (name, str(e)))
                continue
            # Write the secondary profiles
            logging.info('%s: saving next band at: %.1f' % (name, time() - start))
            self.WriteProf(isophoteextract_res, saveto[i], pixscale, starmask = starmask_res, background = background_res, name = name, **kwargs)
        # Best attempt at memory managment...
        del dat
        del starmask_res
        del isophotefit_res
        logging.info('%s: Processing Complete! (at %.1f)' % (name, time() - start))
        return 0
    
    def Process_List(self, filelist, seeing, pixscale, n_procs = 4, saveto = None, names = None, overflowval = None, **kwargs):
        """
        Wrapper function to run "Process_Image" in parallel for many images.
        
        filelist: list of strings or list of list of strings containing image file paths
        seeing: image seeing, if a single value is provided then it is used for all images
        pixscale: angular pixel size in arcsec/pixel
        n_procs: number of processors to use
        saveto: list of strings or list of list of strings containing file paths to save profiles
        names: names of the galaxies, used for logging
        overflowval: pixel flux value for overflow pixels
        """
        
        # Format the inputs so that they can be zipped with the images files
        # and passed to the Process_Image function.
        if type(seeing) in [float, int]:
            use_seeing = [float(seeing)]*len(filelist)
        else:
            use_seeing = seeing
        if type(pixscale) in [float, int]:
            use_pixscale = [float(pixscale)]*len(filelist)
        else:
            use_pixscale = pixscale
        if saveto is None:
            use_saveto = [None]*len(filelist)
        else:
            use_saveto = saveto
        if names is None:
            use_names = [None]*len(filelist)
        else:
            use_names = names
        if overflowval is None:
            use_overflowval = [None]*len(filelist)
        elif type(overflowval) in [int, float]:
            use_overflowval = [overflowval]* len(filelist)
        else:
            use_overflowval = overflowval
            
        # Track how long it takes to run the analysis
        start = time()
        
        # Create a multiprocessing pool to parallelize image processing
        imagedata = list(zip(filelist, use_seeing,
                             use_pixscale, use_saveto,
                             use_names, use_overflowval,
                             [kwargs]*len(filelist)))
        if n_procs > 1:
            with Pool(n_procs) as pool:
                res = pool.starmap(self.Process_Image,
                                   imagedata,
                                   chunksize = 5 if len(filelist) > 20 else 1)
        else:
            res = list(starmap(self.Process_Image, imagedata))
            
        # Report completed processing, and track time used
        logging.info('All Images Finished Processing at %.1f' % (time() - start))
        
        # Return the success/fail indicators for every Process_Image excecution
        return res
                
        

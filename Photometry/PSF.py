from photutils import DAOStarFinder, IRAFStarFinder
import numpy as np
from scipy.stats import iqr
import matplotlib.pyplot as plt
from astropy.visualization import SqrtStretch, LogStretch
from astropy.visualization.mpl_normalize import ImageNormalize
import logging


def Calculate_PSF(IMG, seeing, pixscale, background, name, **kwargs):
    """
    Idenitfy the location of stars in the image and calculate
    their average PSF.

    IMG: numpy 2d array of pixel values
    seeing: the seeing conditions that the image was taken in (arcsec)
    pixscale: conversion factor from pixels to arcsec (arcsec pixel^-1)
    background: output from a image background signal calculation (dict)
    """

    # Guess for PSF based on user provided seeing value
    fwhm_guess = max(seeing / pixscale, 1)

    # photutils wrapper for IRAF star finder
    iraffind = IRAFStarFinder(fwhm = fwhm_guess, threshold = 20.*background['iqr'])
    irafsources = iraffind(IMG - background['median'])

    logging.info('%s: found psf: %f' % (name,np.median(irafsources['fwhm'])))

    # Return PSF statistics
    return {'median': np.median(irafsources['fwhm']),
            'mean': np.mean(irafsources['fwhm']),
            'std': np.std(irafsources['fwhm']),
            'iqr': iqr(irafsources['fwhm'])}
    

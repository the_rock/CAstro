import numpy as np
from photutils.isophote import EllipseSample, EllipseGeometry, Isophote, IsophoteList
from photutils.isophote import Ellipse as Photutils_Ellipse
from scipy.optimize import minimize
from scipy.stats import iqr
from time import time
from astropy.visualization import SqrtStretch, LogStretch
from astropy.visualization.mpl_normalize import ImageNormalize
import matplotlib.pyplot as plt
from matplotlib.patches import Ellipse
from copy import copy
from scipy.fftpack import fft, ifft
import logging

def _iso_extract(IMG, sma, eps, pa, c, more = False):
    """
    Internal, basic function for extracting the pixel fluxes along and isophote
    """

    # Geometry of the isophote
    geo = EllipseGeometry(x0 = c['x'], y0 = c['y'],
                          sma = sma,
                          eps = eps,
                          pa = pa)
    # Extract the isophote information
    ES = EllipseSample(IMG,
                       sma = sma,
                       geometry = geo)
    ES.extract()
    # Return the desited vlaues, either just SB values,
    # or SB values and angles
    if more:
        return ES.values[2], ES.values[0]
    else:
        return ES.values[2]

def _x_to_pa(x):
    """
    Internal, basic function to ensure position angles remain
    in the proper parameter space (0, pi)
    """
    return x % np.pi #np.pi / (1. + np.exp(-(x-np.pi/2)))
def _inv_x_to_pa(pa):
    """
    Internal, reverse _x_to_pa, although this is just a filler
    function as _x_to_pa is non reversible.
    """
    return pa

def _x_to_eps(x):
    """
    Internal, function to map the reals to the range (0.05,0.95)
    as the range of reasonable ellipticity values.
    """
    return 0.05 + 0.95/(1. + np.exp(-(x - 0.5))) #return 0.01 + 0.98/(1. + np.exp(-(x - 0.5)))
def _inv_x_to_eps(eps):
    """
    Internal, inverse of _x_to_eps function
    """
    return 0.5 - np.log(0.95/(eps - 0.05) - 1.) # return 0.5 - np.log(0.98/(eps - 0.01) - 1.)


def _photutils_init_loss(IMG, a, center, x0):
    """
    Internal, function used to compute a basic loss function for
    initializing the photutils automated isophote fitting code.
    """
    iso_loss = 0

    for e_i in range(len(a)):
        # Loss for isophote accuracy
        ES = _iso_extract(IMG, a[e_i], x0[0], x0[1], center)
        
        # iso_values.append([copy(ES.values[0]), copy(ES.values[2])])
        if len(ES) > 5 and np.all(np.isfinite(ES)):
            scat = np.max(np.abs(fft(ES)[1:3])) / len(ES)
            iso_loss += scat if np.isfinite(scat) else 1.
        else:
            logging.warning('%s: iso values failed!' % name)
            iso_loss = np.inf
            del ES
            break
        del ES
    return iso_loss

    

def _polyexp(x, r):
    """
    Internal, function for evaluating the polynomial form of
    the pa and ellipticity profiles
    """
    return (x[0] - x[1] + x[2]*r + x[3]*r**2 + x[4]*r**3)*np.exp(-r/x[5]) + x[1]

def _PolyIso_Loss(x, IMG, c, a, iso_norm = 1., name = ''):
    """
    Not for use by user, built as a loss function for optimizing the
    elliptical isophotes
    """
    iso_loss = 0
    reg_loss = 0

    EPS = _polyexp(x[:6], a)
    PA = _polyexp(x[6:], a)

    logging.info('ellipticities: %s' % str(list(EPS)))
    
    for i in range(len(EPS)):
        # Loss for isophote accuracy
        # iso_values.append([copy(ES.values[0]), copy(ES.values[2])])
        ES = _iso_extract(IMG, a[i], _x_to_eps(EPS[i]), _x_to_pa(PA[i]), c)
        
        if len(ES) > 5 and np.all(np.isfinite(ES)):
            scat = np.sum(np.abs(fft(ES)[1:3])*np.array([1.,0.2])) / len(ES)
            iso_loss += scat
        else:
            logging.warning('%s: iso values failed!' % name)
            iso_loss = np.inf
            break
        
        del ES
    reg_loss += np.sum(x[2:5]**2) + np.sum(x[8:11]**2)
    # reg_loss += max(x[0],-10)

    logging.info('loss: %f, individual: %f %f' % (iso_loss + reg_loss, iso_loss, reg_loss))
    return iso_loss + reg_loss

def PolyIso(IMG, seeing, pixscale, background, psf, mask, center, name, **kwargs):
    """
    Computes an isophote profile using a polynomial form for the pa
    and ellipticity
    """
    dat = IMG - background['median']

    # Select at which radii to sample ellipses for fitting
    if 'scale' in kwargs:
        scale = kwargs['scale']
    else:
        scale = 0.5
    ellipse_radii = [seeing/pixscale]
    while ellipse_radii[-1] < (len(IMG)/2):
        ellipse_radii.append(ellipse_radii[-1]*(1+scale))
        if np.median(_iso_extract(dat,ellipse_radii[-1],0.,0.,center)) < background['iqr']:
            break
    ellipse_radii = np.array(ellipse_radii)
    logging.info('%s: radial steps: %i' % (name, len(ellipse_radii)))

    # Large scale fit with constant pa and ellipticity via grid search
    initializations = []
    N_e, N_pa = 7, 20
    for e in np.linspace(0.2,0.8,N_e): 
        for p in np.linspace(0., (N_pa-1)*np.pi/N_pa,N_pa): 
            initializations.append((e,p))
    best = [np.inf, []]
    logging.info('%s: Initializing' % name)
    for e,p in initializations:
        logging.debug('%s: %.2f, %.2f' % (name,e,p))
        result = _photutils_init_loss(dat, ellipse_radii, center, (e,p))
        if result < best[0]:
            logging.info('%s: new best init loss: %s %f' % (name, str((e,p)), result))
            best[0] = result
            best[1] = copy((e,p))
        logging.debug('%s: best: %f %s' % (name,best[0], str(best[1])))
    
    x0 = np.array([_inv_x_to_eps(0.1), _inv_x_to_eps(best[1][0]), 0., 0., 0., ellipse_radii[1]*2,
                   _inv_x_to_pa(best[1][1]), _inv_x_to_pa(best[1][1]), 0., 0., 0., ellipse_radii[1]*2])
    
    logging.info('%s: Initialize isophotes: %s' % (name, str(list(x0))))
    logging.info(str(list(_x_to_eps(_polyexp(x0, ellipse_radii)))))
    X = minimize(_PolyIso_Loss, x0 = x0, args = (IMG, center, ellipse_radii), method = 'SLSQP', options = {'eps':1e-5})
    print(X)
    logging.info(X.message)
    logging.info(list(X.x))
    return {'X': np.array([list(_polyexp(X.x[:6], ellipse_radii)), list(_polyexp(X.x[6:], ellipse_radii))]),
            'ellipse_radii': list(ellipse_radii)}


def Photutils_Fit(IMG, seeing, pixscale, background, psf, mask, center, name, **kwargs):
    """
    Function to run the photutils automated isophote analysis on an image.
    """
    
    if 'scale' in kwargs:
        scale = kwargs['scale']
    else:
        scale = 0.5
        
    dat = IMG - background['median']
    dat[dat <= 0] = 1e-9
    dat = np.log10(dat)

    ellipse_radii = [5*seeing/pixscale]
    while ellipse_radii[-1] < (len(IMG)/2):
        ellipse_radii.append(ellipse_radii[-1]*(1+scale))
    ellipse_radii.pop()
    logging.info('%s: radial steps: %i' % (name, len(ellipse_radii)))

    # Large scale fit with constant pa and ellipticity via grid search
    initializations = []
    N_e, N_pa = 7, 20
    for e in np.linspace(0.05,0.95,N_e): 
        for p in np.linspace(0., (N_pa-1)*np.pi/N_pa,N_pa): 
            initializations.append((e,p))
    best = [np.inf, []]
    logging.info('%s: Initializing' % name)
    for e,p in initializations:
        logging.debug('%s: %.2f, %.2f' % (name,e,p))
        result = _photutils_init_loss(dat, ellipse_radii, center, (e,p))
        if result < best[0]:
            logging.info('%s: new best init loss: %s %f' % (name, str((e,p)), result))
            best[0] = result
            best[1] = copy((e,p))
        logging.debug('%s: best: %f %s' % (name,best[0], str(best[1])))

    logging.info('%s: best ellipse: %s' % (name, str(best[1])))
    iso_vals = []
    for a in ellipse_radii:
        geo = EllipseGeometry(x0 = center['x'], y0 = center['y'],
                              sma = a,
                              eps = best[1][0],
                              pa = best[1][1])
        ES = EllipseSample(dat,
                           sma = a,
                           geometry = geo)
        ES.extract()
        if len(ES.values[2]) > 5 and np.all(np.isfinite(ES.values[2])):
            iso_vals.append(10**np.median(ES.values[2]))
        else:
            iso_vals.append(-1)

    N = np.argmin(np.abs(iso_vals[0]*0.1 - np.array(iso_vals)))

    logging.info('%s: best initial radius: %f at index %i' % (name, ellipse_radii[N], N))
    logging.info('%s: iso_vals: %s' % (name, str(iso_vals)))
    
    geo = EllipseGeometry(x0 = center['x'], y0 = center['y'],
                          sma = ellipse_radii[N],
                          eps = best[1][0],
                          pa = best[1][1])
    ellipse = Photutils_Ellipse(dat,
                                geometry = geo)

    isolist = ellipse.fit_image(fix_center = True, linear = False)

    if len(list(isolist.sma)) <= 5:
        logging.info('%s: Photutils fit failed, using uniform photometry' % name)
        return {'X': np.array([[_inv_x_to_eps(best[1][0])]*len(ellipse_radii), [_inv_x_to_pa(best[1][1])]*len(ellipse_radii)]),
                'ellipse_radii': ellipse_radii}
    else:
        return {'X': np.array([list(_inv_x_to_eps(isolist.eps)), list(_inv_x_to_pa(isolist.pa))]),
                'ellipse_radii': list(isolist.sma)}

def _iso_loss(x, IMG, a, c):
    """
    Not for use by user, built as a loss function for optimizing the
    initial elliptical isophote
    """

    ES = EllipseSample(IMG,
                       sma = a,
                       x0 = c['x'],
                       y0 = c['y'],
                       eps = 0.05+0.9*((np.arctan(x[0]) + np.pi/2)/np.pi),
                       position_angle = 0.1 + (np.pi-0.2)*((np.arctan(x[1]) + np.pi/2)/np.pi))
    ES.extract()
    return np.std(ES.values[2]) #+ np.abs((np.quantile(ES.values[2], 0.75) - np.quantile(ES.values[2], 0.25))/2.)

def Isophote_Initialize(IMG, seeing, pixscale, background, mask, center, **kwargs):
    """
    Generate an initial elliptical isophote. Attempt to find a position
    angle and ellipticity that best minimizes the scatter of surface
    brightness values along an ellipse of fixed semi-major axis length.
    
    IMG: numpy 2d array of pixel values
    seeing: the seeing conditions that the image was taken in (arcsec)
    pixelscale: conversion factor from pixels to arcsec (arcsec pixel^-1)
    background: output from a image background signal calculation (dict)
    mask: output from mask calculations, removing stars from the image (dict)
    center: output from galaxy center finding calculation (dict)
    """
    
    dat = np.ma.masked_array(IMG - background['median'], mask['mask'])
    init_steps = [np.linspace(-2.,2.,3),
                  np.linspace(-2.,2.,3)]
    init = []
    best = [np.inf, -1]
    for e in init_steps[0]:
        for p in init_steps[1]:
            try:
                init.append(minimize(_iso_loss, x0 = (e, p), args = (dat, 30*seeing/pixscale, center), method = 'Nelder-Mead')) # bounds = ((0.05,0.95), (0.1, np.pi-0.1))
            except:
                continue
            if init[-1].fun < best[0]:
                best[0] = init[-1].fun
                best[1] = int(len(init)-1)

    init_eps = 0.05+0.9*((np.arctan(init[best[1]].x[0]) + np.pi/2)/np.pi)
    init_pa = 0.1 + (np.pi-0.2)*((np.arctan(init[best[1]].x[1]) + np.pi/2)/np.pi)

    init_geometry = EllipseGeometry(x0 = center['x'], y0 = center['y'],
                                    sma = 30*seeing/pixscale, eps = init_eps, pa = init_pa)
    return init_geometry

def Isophote_Fit(IMG, seeing, pixscale, background, mask, center, **kwargs):
    """
    Old function fitting isophotes using photutils
    """
    dat = np.ma.masked_array(IMG - background['median'], mask['mask'])
    init_geometry = Isophote_Initialize(IMG, seeing, pixscale, background, mask, center, **kwargs)
    ellipse = Ellipse(dat, init_geometry)
    iso_list = ellipse.fit_image(minsma = seeing/pixscale,
                                 step = 0.1,
                                 minit = 10,
                                 maxit = 50,
                                 sclip = 3.,
                                 nclip = 0,
                                 linear = False,
                                 fix_center = True,
                                 fflag = 0.7,
                                 maxgerr = 0.5,
                                 sma0 = 30*seeing/pixscale)
    
    
    return iso_list



def _Isophotes_Simultaneous_Fit_Loss(x, IMG, a, c, x0, x_i, iso_norm = 1., decay_scale = 1., fftlim = 7, name = ''):
    """
    Not for use by user, built as a loss function for optimizing the
    elliptical isophotes. Computes the loss as a function of even
    Fourier coefficients, regularized to match neighbour points in
    the profile. 
    """
    iso_loss = 0
    reg_loss = 0

    # Construct pa and ellipticity profile from input. Zeroth element
    # of x0 applies to whole profile.
    if x_i == 0:
        X = np.array(x0[1:]) + np.array(x) + np.array(x0[0])
    else:
        X = np.array(x0[1:]) + np.array(x0[0])
        X[x_i-1] += np.array(x)

    # Build in circular isophotes at center
    X[:,0] += -10*np.exp(-a/decay_scale)

    # Determine if looping through whole profile or only processing single isophote
    loop_elements = [x_i-1] if x_i > 0 else range(len(a))
    iso_values = []
    for e_i in loop_elements:
        # Extract SB values (in flux) around isophote
        ES = _iso_extract(IMG, a[e_i], _x_to_eps(X[e_i][0]), _x_to_pa(X[e_i][1]), c)

        # Detect failed isophotes that do not have enough points or return nan
        if len(ES) > 10 and np.all(np.isfinite(ES)):
            # Compute Fourier coefficients amplitude
            scat = np.sum(np.abs(fft(np.clip(ES, a_max = np.quantile(ES,0.9), a_min = None))[2:fftlim:2])) / len(ES)
            iso_loss += scat if np.isfinite(scat) else (1./iso_norm) 
        else:
            logging.warning('%s: iso values failed!' % name)
            iso_loss = np.inf
            break
        # Attempt at memory saving
        del ES

        # Add regularization term, penalizing isophotes for deviating from each other
        if e_i > 0 and x_i > 0:
            reg_loss += (((X[e_i][1] - X[e_i-1][1])/0.5)**2)/(4. if e_i != (len(a)-1) else 2.)
            reg_loss += (((_x_to_eps(X[e_i][0]) - _x_to_eps(X[e_i-1][0]))/0.4)**2)/(4. if e_i != (len(a)-1) else 2.)
        elif e_i < (len(a) - 1) and x_i > 0:
            reg_loss += (((X[e_i][1] - X[e_i+1][1])/0.5)**2)/(4. if e_i != 0 else 2.)
            reg_loss += (((_x_to_eps(X[e_i][0]) - _x_to_eps(X[e_i+1][0]))/0.4)**2)/(4. if e_i != 0 else 2.)
        # Add regularization term, penalizing central isophote for being too narrow
        if (1 - _x_to_eps(X[e_i][0])) < 0.1:
            reg_loss += 100

    # Log losses in debug mode for tracing
    if np.random.randint(10) == 0:
        logging.debug('%s: loss: %.3i, %.3f, %.3f, %.3f' % (name, x_i, iso_loss*iso_norm, reg_loss, iso_loss*iso_norm + reg_loss))
    # Plot isophotes for testing purposes
    if x_i == 0 and name != '':
        plt.imshow(IMG, origin = 'lower', cmap = 'Greys_r', norm = ImageNormalize(stretch=LogStretch())) 
        plt.text(0.1,0.9, 'Loss: %.4f, e: %.3f, p: %.3f' % (iso_loss*iso_norm + reg_loss,
                                                            _x_to_eps(x0[0][0]),
                                                            _x_to_pa(x0[0][1])),
                 transform=plt.gca().transAxes, fontdict = {'color':'y'})
        for i in range(len(a)):
            plt.gca().add_patch(Ellipse((c['x'],c['y']), 2*a[i], 2*a[i]*(1. - _x_to_eps(X[i][0])),
                                        _x_to_pa(X[i][1])*180/np.pi, fill = False, linewidth = 0.2, color = 'y'))
        plt.savefig('plots/loss_ellipse_%s.png' % (name))
        plt.clf()        
    
    return iso_loss*iso_norm + reg_loss

def Isophotes_Simultaneous_Fit(IMG, seeing, pixscale, background, psf, mask, center, name, **kwargs):
    """
    Primary function for computing isophotes for a galaxy image. Works
    by minimizing fourier coefficients of surface brightness values
    evaluated around an elliptical isophote. Also some regularization
    is used to prevent profiles from having large variations.

    IMG: numpy 2d array of pixel values
    seeing: the seeing conditions that the image was taken in (arcsec)
    pixelscale: conversion factor from pixels to arcsec (arcsec pixel^-1)
    background: output from a image background signal calculation (dict)
    psf: point spread function information (dict)
    mask: output from mask calculations (dict)
    center: center of the image (dict)
    name: name of the galaxy
    
    returns: specifications for isophote profile (dict)
    """

    # Growth scale for anchor isophote radii, each next radius will
    # be scaled by this factor (plus 1).
    if 'scale' in kwargs:
        scale = kwargs['scale']
    else:
        scale = 0.3

    # subtract background from image during processing
    dat = IMG - background['median']

    ######################################################################
    # Find global ellipticity and position angle.
    # Initial attempt to find size of galaxy in image
    # based on when isophotes SB values start to get
    # close to the background noise level
    init_ellipse_radii = [seeing/pixscale]
    init_scale = seeing/pixscale
    while init_ellipse_radii[-1] < (len(IMG)/2):
        init_ellipse_radii.append(init_ellipse_radii[-1]*(1+scale))
        # Stop when at 10 time background noise
        if np.quantile(_iso_extract(dat,init_ellipse_radii[-1],0.,0.,center), 0.9) < 10*background['iqr']:
            break
    init_scale = init_ellipse_radii[-1] / 30.
    logging.info('%s: init scale: %f' % (name, init_scale))
    init_ellipse_radii = np.array(init_ellipse_radii)

    ######################################################################
    # Large scale fit with constant pa and ellipticity via grid search.
    # simultaneously fits at all scales as rough galaxy radius is not
    # very accurate yet.

    # Make list of pa and ellipticity values to use for grid search
    initializations = []
    N_e, N_pa = 10, 20
    for e in np.linspace(_inv_x_to_eps(0.2),_inv_x_to_eps(0.8),N_e): 
        for p in np.linspace(0., (N_pa-1)*np.pi/N_pa,N_pa): 
            initializations.append((e,p))

    # Cycle through pa and ellipticity values and compute loss
    best = [np.inf, -1, []]
    results = []
    logging.info('%s: Initializing' % name)
    for e,p in initializations:
        logging.debug('%s: %.2f, %.2f' % (name,e,p))
        x0 = [(e,p)] + list((0.,0.) for i in range(len(init_ellipse_radii)))
        results.append(_Isophotes_Simultaneous_Fit_Loss((0.,0.), dat, init_ellipse_radii, center, x0, 0, decay_scale = init_scale, fftlim = 7))
        # Track best pa/ellipticity combination so far
        if results[-1] < best[0]:
            best[0] = results[-1]
            best[1] = int(len(results)-1)
            best[2] = copy(x0)
            logging.debug('%s: best: %f %s' % (name,best[0], str(best[2][0])))
    logging.info('%s: best initialization: %s' % (name, str(best[2][0])))

    ######################################################################
    # Now with accurate pa and ellipticity, grow radius according to "scale"
    # until SB value drops below 10 times background noise
    ellipse_radii = [5*seeing/pixscale]
    toosmall = False
    while ellipse_radii[-1] < (len(IMG)/2):
        # Grow radius
        ellipse_radii.append(ellipse_radii[-1]*(1+scale))
        # Evaluate SB
        iso_scale = np.median(_iso_extract(dat,ellipse_radii[-1],_x_to_eps(best[2][0][0]),_x_to_pa(best[2][0][1]),center))
        logging.info('%s: ellipse radii scale %.5f background %.5f' % (name, iso_scale, background['iqr']))
        # Check for early exit condition, indicating small galaxy
        if iso_scale < 10*background['iqr'] and len(ellipse_radii) <= 7:
            toosmall = True
        # Check that SB has dropped close to noise level
        if iso_scale < 10*background['iqr'] and len(ellipse_radii) > 7:
            break
    ellipse_radii = np.array(ellipse_radii)
    # Set decay scale for ellipticity initialization
    decay_scale = ellipse_radii[-1] / 30.

    # If earlier check identifies small galaxy, redo radius growth with
    # a smaller growth scale to allow for more useful samples of the
    # galaxy brightness.
    if toosmall:
        # Start at smallest reasonable radius (one seeing length)
        ellipse_radii = [seeing/pixscale]
        while ellipse_radii[-1] < (len(IMG)/2):
            # Grow radius with smaller scale
            ellipse_radii.append(ellipse_radii[-1]*(1+(scale/2.)))
            # Evaluate SB
            iso_scale = np.median(_iso_extract(dat,ellipse_radii[-1],_x_to_eps(best[2][0][0]),_x_to_pa(best[2][0][1]),center))
            logging.info('%s: ellipse radii scale %.5f background %.5f' % (name, iso_scale, background['iqr']))
            # Check for low signal, now only 5 times background level to go deeper
            if iso_scale < 5*background['iqr'] and len(ellipse_radii) > 7:
                break
        ellipse_radii = np.array(ellipse_radii)
        # Set decay scale for ellipticity initialization
        decay_scale = ellipse_radii[-1] / 30.
        
    logging.info('%s: radial steps: %i, end radius: %f' % (name, len(ellipse_radii), ellipse_radii[-1]*pixscale))

    ######################################################################
    # Get normalizations to even out optimization so it doesn't solely
    # focus on the center of the galaxy with high S/N
    iso_norms = [1.]
    for i in range(1,len(ellipse_radii)+1):
        iso_norms.append(10./np.abs(_Isophotes_Simultaneous_Fit_Loss((0,0), dat, ellipse_radii, center,
                                                                     [(best[2][0][0],best[2][0][1])] + \
                                                                     list((0.,0.) for i in range(len(ellipse_radii))),
                                                                     i, decay_scale = decay_scale, fftlim = 5)))
        if not np.isfinite(iso_norms[-1]):
            iso_norms[-1] = 1.
            
    iso_norms[0] = np.median(iso_norms[1:])
    # Adjust iso_norms to prefer center be controlled amount
    iso_norms = np.array(iso_norms) / np.logspace(0.,1.,len(ellipse_radii)+1)
    logging.debug('%s: iso norms: %s' % (name,str(iso_norms)))

    # initialize container for isophote profile
    X = np.array(list((0.,0.) for i in range(len(ellipse_radii)+1)))
    X[0] = np.array(best[2][0])
    logging.debug('%s: result X: %s' %(name,str(X)))

    # # Run loss and plot isophote initialization for diagnostic purposes
    # _Isophotes_Simultaneous_Fit_Loss((0.,0.), dat, ellipse_radii, center, X, 0, iso_norms[0], decay_scale = decay_scale, name = name + '_init')

    ######################################################################
    # Scale for purturbations of the isophote pa and ellipticity
    perturb_scale = np.array([0.03, 0.06])
    # Number of perturbations for a single isophote each cycle
    N_perturb = 4

    # Fine tune individual isophotes
    new_X = copy(X)
    count = 0
    losses = []
    best = [np.inf, new_X]
    # Loop 300 times or until exit condition
    while count < 300:
        # Periodically include logging message
        if count % 10 == 0:
            logging.info('%s: count: %i' % (name,count))
        old_X = copy(new_X)
        # Alternate direction for cycling through the isophotes (inner isophote first vs outer isophote first)
        iso_order = list(range(1,len(ellipse_radii)+1)) if (count % 2) == 0 else list(reversed(range(1,len(ellipse_radii)+1)))
        for i in iso_order:
            logging.debug('%s: iso order: %i' % (name,i))

            # Track isophote perturbation evaluations, starting
            # with the isophote initial parameters
            temp_best = {'fun': [_Isophotes_Simultaneous_Fit_Loss((0.,0.), dat, ellipse_radii, center, new_X, i, iso_norms = iso_norms[i], decay_scale = decay_scale, fftlim = 5)],
                         'X': [copy(new_X)]}
            
            logging.debug('%s: made temp_best' % name)
            for n in range(N_perturb):
                # generate a random perturbation for the isophote
                shift = np.array([np.random.normal(loc = 0, scale = perturb_scale[0]) * (count % 4),
                                  np.random.normal(loc = 0, scale = perturb_scale[1]) * ((count+2) % 4)])
                # if the current isophote is very far from its neighbor, try resetting it
                if i > 1 and n == 0 and count % 10 == 0 and np.abs(new_X[i][0] - new_X[i-1][0]) > 0.05:
                    shift = new_X[i-1] - new_X[i]
                logging.debug('%s: perturb n: %i %s' % (name,n,str(shift)))
                # Evaluate the perturbed isophote
                temp_best['fun'].append(_Isophotes_Simultaneous_Fit_Loss(shift, dat, ellipse_radii, center, new_X, i, iso_norms = iso_norms[i], decay_scale = decay_scale, fftlim = 5))
                temp_best['X'].append(copy(new_X))
                temp_best['X'][-1][i] += shift
            logging.debug('%s: function evaluations: %s' % (name,str(temp_best['fun'])))
            new_X[i] = copy(temp_best['X'][np.argmin(temp_best['fun'])][i])
            if np.argmin(temp_best['fun']) != 0:
                logging.debug('%s: Improved loss %i on count %i mod4 %i' % (name, i, count, count % 4))
        count += 1
        logging.debug('%s: Recording Loss' % name)
        losses.append(_Isophotes_Simultaneous_Fit_Loss((0.,0.), dat, ellipse_radii, center, new_X, 0, iso_norms[0], decay_scale = decay_scale))
        # Track best performing isophote profile so far
        if losses[-1] < best[0]:
            best[0] = losses[-1]
            best[1] = copy(new_X)
        # exit loop if no improvements are being made
        if len(losses) > 100 and np.all(np.array(losses[-10:]) == losses[-1]):
            logging.info('%s: no change in losses, breaking at count %i' % (name, count))
            break
    X = best[1]

    # Run loss and plot fit for diagnostic purposes
    # _Isophotes_Simultaneous_Fit_Loss((0.,0.), dat, ellipse_radii, center, X, 0, iso_norms[0], decay_scale = decay_scale, name = name + '_fin')
    
    # # Plot progression of fit loss for diagnostic purposes
    # plt.plot(range(len(losses)), losses, linewidth = 2, color = 'k')
    # plt.savefig('plots/Losses_%s.png' % name)
    # plt.clf()

    return {'X': np.array([list((x[0] + X[0][0] - 10*np.exp(-r/decay_scale)) for x,r in zip(X[1:],ellipse_radii)), list((x[1] + X[0][1]) for x in X[1:])]),
            'ellipse_radii': list(ellipse_radii)}


######################################################################
def _Fit_Isophotes_Loss(x, IMG, a, c, x_i, iso_norms = [], fftlim = 7, name = '', **kwargs):
    """
    Not for use by user, built as a loss function for optimizing the
    elliptical isophotes. Computes the loss as a function of even
    Fourier coefficients, regularized to match neighbour points in
    the profile. 
    """
    iso_loss = 0
    reg_loss = 0

    if len(iso_norms) == 0:
        iso_norms = np.ones(len(a))
        
    for i in x_i:
        # Extract SB values (in flux) around isophote
        ES = _iso_extract(IMG, a[i], _x_to_eps(x[i][0]), _x_to_pa(x[i][1]), c)

        # Detect failed isophotes that do not have enough points or return nan
        if len(ES) > 10 and np.all(np.isfinite(ES)):
            # Compute Fourier coefficients amplitude
            scat = 0.5*iqr(ES) + (np.sum(np.abs(fft(np.clip(ES, a_max = np.quantile(ES,0.9), a_min = None))[2:fftlim:2])) / len(ES))
            iso_loss += iso_norms[i]*scat if np.isfinite(scat) else np.inf 
        else:
            logging.warning('%s: iso values failed!' % name)
            iso_loss = np.inf
            break

        # Add regularization term, penalizing isophotes for deviating from each other
        if i > 0:
            reg_loss += (((x[i][1] - x[i-1][1])/0.4)**2)/(4. if i != (len(a)-1) else 2.)
            reg_loss += (((_x_to_eps(x[i][0]) - _x_to_eps(x[i-1][0]))/0.3)**2)/(4. if i != (len(a)-1) else 2.)
        elif i < (len(a) - 1):
            reg_loss += (((x[i][1] - x[i+1][1])/0.4)**2)/(4. if i != 0 else 2.)
            reg_loss += (((_x_to_eps(x[i][0]) - _x_to_eps(x[i+1][0]))/0.3)**2)/(4. if i != 0 else 2.)
        # Add regularization term, penalizing very narrow isophotes
        if (1 - _x_to_eps(x[i][0])) < 0.1:
            reg_loss += 10

    # bias the last isophote away form circular to counteract noise floor effect
    reg_loss += 0.1*(1. - _x_to_eps(x[-1][0]))
        
    # Log losses in debug mode for tracing
    if np.random.randint(10) == 0:
        logging.debug('%s: loss: %.3f, %.3f, %.3f' % (name, iso_loss, reg_loss, iso_loss + reg_loss))
    # Plot isophotes for testing purposes
    if len(x_i) > 1 and name != '' and 'doplot' in kwargs and kwargs['doplot']:
        plt.imshow(IMG, origin = 'lower', cmap = 'Greys_r', norm = ImageNormalize(stretch=LogStretch())) 
        plt.text(0.1,0.9, 'Loss: %.4f, e: %.3f, p: %.3f' % (iso_loss + reg_loss,
                                                            _x_to_eps(x[0][0]),
                                                            _x_to_pa(x[0][1])),
                 transform=plt.gca().transAxes, fontdict = {'color':'y'})
        for i in range(len(a)):
            plt.gca().add_patch(Ellipse((c['x'],c['y']), 2*a[i], 2*a[i]*(1. - _x_to_eps(x[i][0])),
                                        _x_to_pa(x[i][1])*180/np.pi, fill = False, linewidth = 0.2, color = 'y'))
        plt.savefig('%sloss_ellipse_%s.png' % (kwargs['plotpath'] if 'plotpath' in kwargs else '', name))
        plt.clf()        
    
    return iso_loss + reg_loss

def _Fit_Isophotes_Initialize(Rinit, dat, scale, center, background_noise, name):
    """
    Determine the global pa and ellipticity for a galaxy
    """
    ######################################################################
    # Find global ellipticity and position angle.
    # Initial attempt to find size of galaxy in image
    # based on when isophotes SB values start to get
    # close to the background noise level
    circ_ellipse_radii = [Rinit]
    while circ_ellipse_radii[-1] < (len(dat)/2):
        circ_ellipse_radii.append(circ_ellipse_radii[-1]*(1+scale))
        # Stop when at 10 time background noise
        if np.quantile(_iso_extract(dat,circ_ellipse_radii[-1],0.,0.,center), 0.9) < 3*background_noise and len(circ_ellipse_radii) > 7:
            break
    logging.info('%s: init scale: %f' % (name, circ_ellipse_radii[-1]))
    circ_ellipse_radii = np.array(circ_ellipse_radii)

    ######################################################################
    # Large scale fit with constant pa and ellipticity via grid search.
    # simultaneously fits at all scales as rough galaxy radius is not
    # very accurate yet.

    # Make list of pa and ellipticity values to use for grid search
    initializations = []
    N_e, N_pa = 10, 20
    for e in np.linspace(_inv_x_to_eps(0.2),_inv_x_to_eps(0.8),N_e): 
        for p in np.linspace(0., (N_pa-1)*np.pi/N_pa,N_pa): 
            initializations.append((e,p))

    # Cycle through pa and ellipticity values and compute loss
    best = [np.inf, -1, []]
    results = []
    logging.info('%s: Initializing' % name)
    for e,p in initializations:
        logging.debug('%s: %.2f, %.2f' % (name,e,p))
        results.append(_Fit_Isophotes_Loss([[e,p]]*len(circ_ellipse_radii), dat,
                                           circ_ellipse_radii, center,
                                           range(len(circ_ellipse_radii)),
                                           iso_norms = [], fftlim = 7))
        # Track best pa/ellipticity combination so far
        if results[-1] < best[0]:
            best[0] = results[-1]
            best[1] = int(len(results)-1)
            best[2] = copy([e,p])
            logging.debug('%s: best: %f %s' % (name,best[0], str(best[2][0])))
            
    logging.info('%s: best initialization: %s' % (name, str(best[2][0])))
    return best[2]

def _Fit_Isophotes_SampleRadii(init_params, Rinit, scale, dat, background_noise, center, name):
    """
    Determine which radii to sample the isophotes at while fitting
    """
    
    ellipse_radii = [Rinit]
    toosmall = False
    while ellipse_radii[-1] < (len(dat)/2):
        # Grow radius
        ellipse_radii.append(ellipse_radii[-1]*(1+scale))
        # Evaluate SB
        iso_scale = np.median(_iso_extract(dat,ellipse_radii[-1],_x_to_eps(init_params[0]),_x_to_pa(init_params[1]),center))
        logging.info('%s: ellipse radii %.3f scale %.5f background %.5f' % (name, ellipse_radii[-1], iso_scale, background_noise))
        # Check for early exit condition, indicating small galaxy
        if iso_scale < 5*background_noise and len(ellipse_radii) <= 7:
            toosmall = True
        # Check that SB has dropped close to noise level
        if iso_scale < 5*background_noise and len(ellipse_radii) > 7:
            break

    # If earlier check identifies small galaxy, redo radius growth with
    # a smaller growth scale to allow for more useful samples of the
    # galaxy brightness.
    if toosmall:
        # Start at smallest reasonable radius (one seeing length)
        count = 1.
        while toosmall and count < 5:
            logging.info('%s: galaxy is small (%i), using finer growth scale for fitting' % (name, count))
            toosmall = False
            ellipse_radii = [Rinit/(1.+2*count)]
            while ellipse_radii[-1] < (len(dat)/2):
                # Grow radius with smaller scale
                ellipse_radii.append(ellipse_radii[-1]*(1+(scale/(1. + 2*count))))
                # Evaluate SB
                iso_scale = np.median(_iso_extract(dat,ellipse_radii[-1],_x_to_eps(init_params[0]),_x_to_pa(init_params[1]),center))
                logging.info('%s: ellipse radii %.3f scale %.5f background %.5f' % (name, ellipse_radii[-1], iso_scale, background_noise))
                # Check for early exit condition, indicating small galaxy
                if iso_scale < 5*background_noise and len(ellipse_radii) <= 7:
                    toosmall = True
                # Check for low signal, now only 5 times background level to go deeper
                if iso_scale < 5*background_noise and len(ellipse_radii) > 7:
                    break
            count += 1
        if count >= 5:
            logging.warning('%s: Galaxy seems very small! Possible failed center. Will process anyway' % name)
    ellipse_radii = np.array(ellipse_radii)
        
    logging.info('%s: radial steps: %i, end radius: %f' % (name, len(ellipse_radii), ellipse_radii[-1]))
    return ellipse_radii

def _Fit_Isophotes_IsoNorms(Xinit, ellipse_radii, dat, center, name):
    iso_norms = [1.]
    for i in range(len(ellipse_radii)):
        iso_norms.append(10./np.abs(_Fit_Isophotes_Loss(Xinit, IMG = dat, a = ellipse_radii,
                                                        c = center, x_i = [i], iso_norms = [],
                                                        fftlim = 5)))
        if not np.isfinite(iso_norms[-1]):
            iso_norms[-1] = 1.
            
    iso_norms[0] = np.median(iso_norms[1:])
    # Adjust iso_norms to prefer center be controlled amount
    iso_norms = np.array(iso_norms) / np.logspace(0.,1.,len(ellipse_radii)+1)
    logging.debug('%s: iso norms: %s' % (name,str(iso_norms)))

    return iso_norms
    

def Fit_Isophotes(IMG, seeing, pixscale, background, psf, mask, center, name, **kwargs):
    """
    Primary function for computing isophotes for a galaxy image. Works
    by minimizing fourier coefficients of surface brightness values
    evaluated around an elliptical isophote. Also some regularization
    is used to prevent profiles from having large variations.

    IMG: numpy 2d array of pixel values
    seeing: the seeing conditions that the image was taken in (arcsec)
    pixelscale: conversion factor from pixels to arcsec (arcsec pixel^-1)
    background: output from a image background signal calculation (dict)
    psf: point spread function information (dict)
    mask: output from mask calculations (dict)
    center: center of the image (dict)
    name: name of the galaxy (string)
    
    returns: specifications for isophote profile (dict)
    """

    # Growth scale for anchor isophote radii, each next radius will
    # be scaled by this factor (plus 1).
    if 'scale' in kwargs:
        scale = kwargs['scale']
    else:
        scale = 0.3

    # subtract background from image during processing
    dat = IMG - background['median']

    ######################################################################
    # Get starting parameters for the isophotes 
    init_params = _Fit_Isophotes_Initialize(Rinit = seeing/pixscale, dat = dat, scale = scale,
                                            center = center, background_noise = background['iqr'],
                                            name = name)

    ######################################################################
    # Now with accurate pa and ellipticity, grow radius according to "scale"
    # until SB value drops below 10 times background noise 
    ellipse_radii = _Fit_Isophotes_SampleRadii(init_params = init_params, Rinit = 10*seeing/pixscale, scale =  scale,
                                               dat = dat, background_noise = background['iqr'], center = center,
                                               name = name)

    ######################################################################
    # initialize container for isophote profile
    X = np.array([init_params]*len(ellipse_radii))
    X[:,0] += -1*np.exp(-(ellipse_radii - ellipse_radii[0])/(ellipse_radii[-1] / len(ellipse_radii)))
    logging.debug('%s: result X: %s' %(name,str(list(X))))
    
    ######################################################################
    # Get normalizations to even out optimization so it doesn't solely
    # focus on the center of the galaxy with high S/N 
    iso_norms = _Fit_Isophotes_IsoNorms(Xinit = X, ellipse_radii = ellipse_radii,
                                        dat = dat, center = center, name = name)

    ######################################################################
    # Scale for purturbations of the isophote pa and ellipticity
    perturb_scale = np.array([0.03, 0.06])
    # Number of perturbations for a single isophote each cycle
    N_perturb = 4

    # Fine tune individual isophotes
    new_X = copy(X)
    count = 0
    losses = []
    best = [_Fit_Isophotes_Loss(x = new_X, IMG = dat, a = ellipse_radii,
                                c = center, x_i =  range(len(ellipse_radii)),
                                iso_norms = iso_norms, fftlim = 7),
            new_X]
    losses = [best[0]]
    # Loop 300 times or until exit condition
    while count < 300:
        # Periodically include logging message
        if count % 10 == 0:
            logging.info('%s: count: %i' % (name,count))
        old_X = copy(new_X)
        # Alternate direction for cycling through the isophotes (inner isophote first vs outer isophote first)
        iso_order = list(range(len(ellipse_radii))) if (count % 2) == 0 else list(reversed(range(len(ellipse_radii))))
        for i in iso_order:
            logging.debug('%s: iso order: %i' % (name,i))

            # Track isophote perturbation evaluations, starting
            # with the isophote initial parameters 
            temp_best = {'fun': [_Fit_Isophotes_Loss(x = new_X, IMG = dat, a = ellipse_radii,
                                                     c = center,x_i = [i], iso_norms = iso_norms,
                                                     fftlim = (3 if count % 4 == 0 else 5))],
                         'X': [copy(new_X)]}
            
            logging.debug('%s: made temp_best' % name)
            for n in range(N_perturb):
                temp_X = copy(new_X)
                # generate a random perturbation for the isophote
                shift = np.array([np.random.normal(loc = 0, scale = perturb_scale[0]) * (count % 4),
                                  np.random.normal(loc = 0, scale = perturb_scale[1]) * ((count+2) % 4)]) / (max(count,150.)/150.)
                # if the current isophote is very far from its neighbor, try resetting it
                if i > 0 and n == 0 and count % 10 == 0 and np.abs(new_X[i][0] - new_X[i-1][0]) > 0.05:
                    shift = new_X[i-1] - new_X[i]
                temp_X[i] += shift
                logging.debug('%s: perturb n: %i %s' % (name,n,str(shift)))
                # Evaluate the perturbed isophote 
                temp_best['fun'].append(_Fit_Isophotes_Loss(x = temp_X, IMG = dat, a = ellipse_radii,
                                                            c = center, x_i = [i], iso_norms = iso_norms,
                                                            fftlim = (3 if count % 4 == 0 else 5)))
                temp_best['X'].append(copy(temp_X))
            logging.debug('%s: function evaluations: %s' % (name,str(temp_best['fun'])))
            new_X[i] = copy(temp_best['X'][np.argmin(temp_best['fun'])][i])
            if np.argmin(temp_best['fun']) != 0:
                logging.debug('%s: Improved loss %i on count %i mod4 %i' % (name, i, count, count % 4))
        count += 1
        if count > 80:
            logging.debug('%s: Recording Loss' % name) # (x, IMG, a, c, x_i, iso_norms = [], fftlim = 7, name = '')
            losses.append(_Fit_Isophotes_Loss(x = new_X, IMG = dat, a = ellipse_radii,
                                              c = center, x_i = range(len(ellipse_radii)),
                                              iso_norms = iso_norms, fftlim = 5))
        # Track best performing isophote profile so far
        if losses[-1] < best[0]:
            best[0] = losses[-1]
            best[1] = copy(new_X)
        # exit loop if no improvements are being made
        if len(losses) > 100 and np.all(np.array(losses[-10:]) == losses[-1]):
            logging.info('%s: no change in losses, breaking at count %i' % (name, count))
            break
    X = best[1]

    if 'doplot' in kwargs and kwargs['doplot']:
        _Fit_Isophotes_Loss(x = new_X, IMG = dat, a = ellipse_radii,
                            c = center, x_i =  range(len(ellipse_radii)),
                            iso_norms = iso_norms, fftlim = 7, name = name,
                            **kwargs)

    return {'X': np.array([X[:,0], X[:,1]]), 'ellipse_radii': list(ellipse_radii)}

import numpy as np
from scipy.integrate import quad as solver
from scipy.special import ellipk, ellipe
import matplotlib.pyplot as plt
import sys
import os
sys.path.append(os.environ['PYTHON_GOODIES'])
from Smoothing import PolySmooth

G = 4.302 # kpc M_sun^-1 (m s^-1)^2


def int_R(Rp, a, Sigma, args):

    return Rp * Sigma(Rp, *args) / np.sqrt(Rp**2 - a**2)

# def int_a(a, z, Sigma, args):

#     plus = np.sqrt(z**2 + (a - R)**2)
#     minus = np.sqrt(z**2 + (a - R)**2)
    
#     return (((a+R) / plus) - ((a - R) / minus)) * solver(int_R, a, np.inf, args = (a, Sigma, args))[0] / np.sqrt(R**2 - z**2 - a**2 + plus * minus)

def int_a(a, R, Sigma, args):

    return solver(int_R, a, np.inf, args = (a, Sigma, args), epsrel = 1e-3)[0] / np.sqrt(R**2 - a**2)

def Phi(R, Sigma, args):

    print R, Sigma, args

    res = []
    for Ri in R:
        print Ri, list(R).index(Ri), len(R)
        res.append(- 4. * G * solver(int_a, 0., Ri, args = (Ri, Sigma, args), epsrel = 1e-1)[0])
        
    return np.array(res)

def exponential_Sigma(R, Sigma0, Rd):

    return Sigma0 * np.exp(- R / Rd)

def interpolated_Sigma(R, R_obs, Sigma_obs, x_inner, r_inner, x_outer, r_outer):
    """
    Interpolates from an arbitraty profile

    R: radius to evaluate Sigma (kpc)
    R_obs: Radii at which the surface density was observed (kpc)
    Sigma_obs: Observed surface density (M_sun kpc^-2)

    returns: interpolated surface density (M_sun kpc^-2)
    """
    if R < r_inner:
        return exponential_Sigma(R, x_inner[0], x_inner[1])
    elif R > r_outer:
        return exponential_Sigma(R, x_outer[0], x_outer[1])
        
    #return PolySmooth(R_obs, Sigma_obs, R, deg = 1, n_points = 5)
    N = np.argsort(np.abs(R - R_obs))
    return np.polyval(np.polyfit(R_obs[N[:8]], Sigma_obs[N[:8]], deg = 1), R)

# test_R = np.linspace(1., 30., 100) # kpc
# test_Rd = 4.9 # kpc

# test_Sigma0 = 1e6 * 50.
# test_phi = Phi(test_R, exponential_Sigma, (test_Sigma0, test_Rd))


# print test_phi

# plt.scatter(test_R, test_phi)
# plt.savefig('deleteme_test_Phi.png')
# plt.clf()

